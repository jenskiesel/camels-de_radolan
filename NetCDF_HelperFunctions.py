import os
try:
    import gdal
    import osr
except ModuleNotFoundError:
    from osgeo import gdal
    from osgeo import osr
import xarray as xr
import numpy as np
import pandas as pd

import hf_GDAL as hfg
import hf


def spatial_subset(ds, lat_min, lat_max, lon_min, lon_max):
    # select lat lon from dataset
    return ds.sel(lat=slice(lat_min, lat_max), lon=slice(lon_min, lon_max))

def spatial_subset_and_concat(nc_var_fps, y_min, y_max, x_min, x_max, concat_dim):
    ds_lst = []
    for fp in nc_var_fps:
        ds = xr.open_dataset(str(fp))
        ds_lst.append(spatial_subset(ds, y_min, y_max, x_min, x_max))
    return xr.concat(ds_lst, dim=concat_dim)

def spatial_subset_bbox_and_merge(nc_fps, bbox):
    # all nc_fps must have same time and space dimension
    ds_lst = []
    for fp in nc_fps:
        ds = xr.open_dataset(str(fp))
        ds_lst.append(spatial_subset_bbox_xy(ds, bbox))
    return xr.merge(ds_lst)

def spatial_subset_bbox_xy(ds, bbx):
    try:
        return ds.sel(X=slice(bbx[0], bbx[2]), Y=slice(bbx[1], bbx[3]))
    except ValueError:
        try:
            return ds.sel(x=slice(bbx[0], bbx[2]), y=slice(bbx[1], bbx[3]))
        except ValueError:
            try:
                return ds.sel(lon=slice(bbx[0], bbx[2]), lat=slice(bbx[1], bbx[3]))
            except ValueError:
                try:
                    return ds.sel(longitude=slice(bbx[0], bbx[2]), latitude=slice(bbx[1], bbx[3]))
                except ValueError:
                    raise ValueError("dimensions or multi-index levels ['X', 'Y'] or ['x', 'y'] or ['lon', 'lat']"
                                     "or ['longitude', 'latitude'] do not exist")

def filter_nc_files(all_files):
    #
    # this function filters the files so that only netCDF files are processed
    #
    nc_list = []
    for afile in all_files:
        if os.path.splitext(afile)[1] == '.nc':
            nc_list.append(afile)
    return nc_list

def get_data_from_nc_closest_lon_lat(nc, lon, lat, var_str, lat_str='latitude', lon_str='longitude'):
    # this function extracts the closest data ('var_str')
    # from a netCDF at location lon, lat
    ds = xr.open_dataset(nc)
    kwargs = {lat_str: lat, lon_str: lon}
    return ds.sel(**kwargs, method='nearest').variables[var_str].data

def get_df_from_ds_closest_lon_lat(ds, lon, lat, vars, lat_str='latitude', lon_str='longitude'):
    kwargs = {lat_str: lat, lon_str: lon}
    df = ds[vars].sel(**kwargs, method='nearest').to_dataframe()
    return df[vars]

def get_geotransform_nx_ny_from_bbox(min_lon, min_lat, max_lon, max_lat, res):
    """
    this function returns the geotransform and nx and ny from a bounding box
    and the resolution (res)
    """
    nx = int(float((max_lon-min_lon)/res))+1
    ny = int(float((max_lat-min_lat)/res))+1
    xres = res
    yres = res
    geotransform = (min_lon, xres, 0, max_lat, 0, -yres)
    return geotransform, nx, ny


def get_geotransform_nx_ny(nc_obj, lon='lon', lat='lat'):
    """
    this function creates and returns the geotransform list from a net cdf object
    and also returns the number of x and y entries
    """
    lat = nc_obj.variables[lat][:]
    lon = nc_obj.variables[lon][:]
    xmin, ymin, xmax, ymax = [min(lon), min(lat), max(lon), max(lat)]

    # set geotransform
    nx = len(lon)
    ny = len(lat)
    xres = (xmax - xmin) / (float(nx)-1)
    yres = (ymax - ymin) / (float(ny)-1)
    geotransform = (xmin, xres, 0, ymax, 0, -yres)
    return geotransform, nx, ny

def get_abs_max_cellsize_nc(nc, x_var_str, y_var_str):
    gt, nx, ny = get_geotransform_nx_ny_fromNetCDF(nc, x_var_str, y_var_str)
    x_res = float(get_x_res(gt))
    y_res = float(get_y_res(gt))
    return max(abs(x_res), abs(y_res))

def get_geotransform_nx_ny_xrds(nc_ds, x_var_str, y_var_str, center_cell, data_type="RAD"):
    """
    this function creates and returns the geotransform list from a net cdf dataset
    and also returns the number of x and y entries
    if center_cell is true, the nc_ds latitude-longitude is centered in the cell
    then it re-sets the center of the NetCDF cell to the lower left corner (-xres and -yres)
    """
    if data_type=="RAD" or data_type == 'radolan':
        lat = nc_ds.variables[y_var_str][:].values.tolist()
        lon = nc_ds.variables[x_var_str][:].values.tolist()
    elif data_type =="TRY":
        lat = nc_ds.variables[y_var_str].values.tolist()
        lon = nc_ds.variables[x_var_str].values.tolist()
    xmin, ymin, xmax, ymax = [min(lon), min(lat), max(lon), max(lat)]

    # set geotransform
    nx = len(lon)
    ny = len(lat)
    try:
        xres = (xmax - xmin) / (float(nx)-1)
    except ZeroDivisionError:
        print("WARNING: only one cell in x-direction left: setting resolution to 1000m")
        xres = 1000
    try:
        yres = (ymax - ymin) / (float(ny)-1)
    except ZeroDivisionError:
        print("WARNING: only one cell in y-direction left: setting resolution to 1000m")
        yres = 1000
    if center_cell:
        geotransform = (xmin-xres/2, xres, 0, ymax-yres/2, 0, -yres)
    else:
        geotransform = (xmin, xres, 0, ymax, 0, -yres)
    return geotransform, nx, ny

def get_geotransform_nx_ny_fromNetCDF(nc_file, x_var_str, y_var_str):
    """
    this function creates and returns the geotransform list from a net cdf object
    and also returns the number of x and y entries
    """
    ds = xr.open_dataset(nc_file)
    lat = ds.variables[y_var_str][:]
    lon = ds.variables[x_var_str][:]
    xmin, ymin, xmax, ymax = [min(lon), min(lat), max(lon), max(lat)]

    # set geotransform
    nx = len(lon)
    ny = len(lat)
    xres = (xmax - xmin) / (float(nx)-1)
    yres = (ymax - ymin) / (float(ny)-1)
    geotransform = (xmin, xres, 0, ymax, 0, -yres)
    return geotransform, nx, ny

def get_x_res(geotransform):
    return geotransform[1]

def get_y_res(geotransform):
    return -geotransform[5]

def extract_bounding_box(in_netcdf, bbox, out_netcdf):
    """
    this function cuts a net CDF at the bounding box and writes to new file
    :param in_netcdf: the NetCDF file path that is to be cut
    :param bbox: a list of [lon_min, lat_min, lon_max, lat_max]
    :param out_netcdf: the cut NetCDF file that is written
    :return:
    """
    ds_in = xr.open_dataset(in_netcdf)
    ds_out = ds_in.sel(lon=(ds_in.lon >= bbox[0]) & (ds_in.lon <= bbox[2]), lat=(ds_in.lat >= bbox[1]) & (ds_in.lat <= bbox[3]))
    ds_out.to_netcdf(out_netcdf)

def subset_nc_by_time(in_netcdf, start_time, end_time, out_netcdf):
    """
    subsets a NetCDF by time
    :param in_netcdf:
    :param start_time: yyyy-mm-dd
    :param end_time: yyyy-mm-dd
    :return:
    """
    ds_in = xr.open_dataset(in_netcdf)
    ds_out = ds_in.sel(time=slice(start_time, end_time))
    ds_out.to_netcdf(out_netcdf)

def aggregate_to_monthly(in_netcdf, out_netcdf):
    """
    this function aggreates a NetCDF to monthly
    :param in_netcdf: the input NetCDF file path
    :param out_netcdf: the output NetCDF file path
    :return:
    """
    ds_in = xr.open_dataset(in_netcdf)
    # use resample - groupby does not keep the dates!!!
    ds_out = ds_in.resample(time='1m').mean()  # http://xarray.pydata.org/en/stable/generated/xarray.Dataset.resample.html
    ds_out.to_netcdf(out_netcdf)


def WriteNumpyToTiff(np_array, geotransform, ny, nx, epsg_code, data_type, outfile):
    if data_type == "float32":
        dst_ds = gdal.GetDriverByName('GTiff').Create(str(outfile), nx, ny, 1, gdal.GDT_Float32)
    elif data_type == "int16":
        dst_ds = gdal.GetDriverByName('GTiff').Create(str(outfile), nx, ny, 1, gdal.GDT_Int16)
    else:
        print("Data type '%s' not supported yet - modify function 'WriteNumpyToTiff'"%(data_type))
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromEPSG(epsg_code)                # WGS84 lat/long
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(np_array)   # write band to the raster
    dst_ds.FlushCache()                     # write to disk


def WriteNumpyToTiffProj4(np_array, geotransform, ny, nx, proj4, data_type, outfile):
    if data_type == "float32":
        dst_ds = gdal.GetDriverByName('GTiff').Create(str(outfile), nx, ny, 1, gdal.GDT_Float32)
    elif data_type == "int16":
        dst_ds = gdal.GetDriverByName('GTiff').Create(str(outfile), nx, ny, 1, gdal.GDT_Int16)
    else:
        print("Data type '%s' not supported yet - modify function 'WriteNumpyToTiff'"%(data_type))
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    srs = osr.SpatialReference()            # establish encoding
    srs.ImportFromProj4(proj4)
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    dst_ds.GetRasterBand(1).WriteArray(np_array)   # write band to the raster
    dst_ds.FlushCache()                     # write to disk


def get_epsg_from_shape(shp_fp):
    shp_prj_fp = os.path.splitext(shp_fp)[0] + '.prj'
    if not os.path.exists(shp_prj_fp):
        raise FileNotFoundError(shp_prj_fp)
    prj_file = open(shp_prj_fp, 'r')
    prj_txt = prj_file.read()
    srs = osr.SpatialReference()
    srs.ImportFromESRI([prj_txt])
    prj = prj_txt
    wkt = srs.ExportToWkt()
    proj4 = srs.ExportToProj4()
    srs.AutoIdentifyEPSG()
    epsg = int(srs.GetAuthorityCode(None))
    return epsg


def nc_to_tif_for_shp(nc_fp, data_var, x_var, y_var, shp_fp, name_field, no_data, out_folder):
    """
    this function creates a tif file for the part(s) of the xarray dataset that is
    covered by a shapefile - resulting tiff(s) can then be used in function
    "mask_ds_from_tif" to subset the ds - IMPORTANT NetCDF and shape have to have
    same projection
    :param InputImage:
    :param shp_fp:
    :param align_ras:
    :param out_folder:
    :return: the filepaths of the resulting tiffs
    """
    EPSG_code = get_epsg_from_shape(shp_fp)
    tmp_ras_fp = os.path.join(os.path.split(nc_fp)[0], 'tmp.tif')
    write_NetCDF_as_mask_to_tiff(nc_fp, data_var, x_var, y_var, 0, EPSG_code, 'float32', tmp_ras_fp)
    ras_fps = hfg.all_features_to_single_rasters(shp_fp, name_field, name_field, tmp_ras_fp,
                                                 no_data, 'float32', out_folder, burn_in_value=0)
    return ras_fps


def mask_ds_from_tif(tif_path, ds_obj, vars_to_mask):
    """
    :param in_tiff: a tiff with value 0 for the area that is to be kept and noData for the rest
                    - created in QGIS from original NetCDF that is read into original ds (cells and extent have to perfectly align)
                      by clipping the NetCDF with a shapefile (keep original Extent) and multiply with 0
    :param ds_obj: the xarray dataset object that is to be masked
    :param vars_to_mask: a list of strings of the variables from ds_obj that are to be masked
    :return:
    """
    ds_tif = gdal.Open(tif_path)
    rb = ds_tif.GetRasterBand(1)
    nodata_val = rb.GetNoDataValue()
    data_arr = np.array(rb.ReadAsArray())
    data_arr[data_arr == nodata_val] = np.nan

    #take lats and lons from ds
    lats = ds_obj.lat
    lons = ds_obj.lon
    if len(lats) == len(data_arr) and len(lons) == len(data_arr[0]):
        # then ds has same extent as the tiff
        dims = list(dict(ds_obj.dims).keys())
        if 'lat' in dims:
            xr_da = xr.DataArray(data_arr, coords=[lats,lons], dims=['lat','lon'])
        elif 'x' in dims:
            xr_da = xr.DataArray(data_arr, coords=[lats,lons], dims=['y', 'x'])
        else:
            print("Dimensions to match lat lon unclear")
            print(dims)
        ds_obj['mask'] = xr_da
        for var in vars_to_mask:
            ds_obj[var] = ds_obj[var] + ds_obj['mask']
        return ds_obj
    else:
        print("ERROR: Lats and Lons of mask do not match size of dataset!")
        return False


def add_area_values_to_lat_lon_ds(xr_ds, lon_var, lat_var):
    """
    function adds a new data variable "area" to the dataset
    that contains the area for each grid cell, lat lon values are assumed to be the center of the grid
    IMPORTANT:
    - Acurracy compared to QGIS seems in the range of <10m
    - Order of lat and lon must be checked when processing new dataset!!!!!
    :param ds: the dataset to which 'area' data is added
    :param lon_var: longitude string
    :param lat_var: latitude string
    :param shape: the shape of the area_m2 array
    """
    gt, nx, ny = get_geotransform_nx_ny(xr_ds, lon_var, lat_var)
    lon_res = get_x_res(gt).values
    lat_res = get_y_res(gt).values

    # optimize through using the array and no loop !!!!!
    area_dict = {}
    area_dict['dims'] = ('lat', 'lon')
    area_dict['coords'] = {}

    area_dict['coords']['lat'] = {}
    area_dict['coords']['lat']['dims'] = ('lat')
    area_dict['coords']['lat']['attrs'] = {'units': 'degrees_north', 'long_name': 'Latitude'},
    area_dict['coords']['lat']['data'] = []

    area_dict['coords']['lon'] = {}
    area_dict['coords']['lon']['dims'] = ('lon')
    area_dict['coords']['lon']['attrs'] = {'units': 'degrees_east', 'long_name': 'Longitude'},
    area_dict['coords']['lon']['data'] = []
    area_dict['data'] = []
    area_dict['name'] = 'area_m2'
    area_dict['attrs'] = {}
    is_first = True
    for lat in xr_ds[lat_var].values:
        area_dict['coords']['lat']['data'].append(lat)
        lon_data = []
        for lon in xr_ds[lon_var].values:
            if is_first:
                area_dict['coords']['lon']['data'].append(lon)
            pt1 = [lon - lon_res/2, lat - lat_res/2]
            pt2 = [lon + lon_res/2, lat - lat_res/2]
            pt3 = [lon - lon_res/2, lat + lat_res/2]
            x_dist = distance_m_between_lat_lon(pt1, pt2)
            y_dist = distance_m_between_lat_lon(pt1, pt3)
            lon_data.append(x_dist * y_dist)
        area_dict['data'].append(lon_data)
        is_first = False
    area_xr_da = xr.DataArray.from_dict(area_dict)
    xr_ds['area_m2'] = area_xr_da

def distance_m_between_lat_lon(pt1, pt2):
    """
    function implements the Haversine formula (0.5% error as opposed to using geopy)
    :param pt1: coordinates of pt 1 in [lon, lat]
    :param pt2: coordinates of pt 2 in [lon, lat]
    :return: distance in m
    """
    from math import sin, cos, sqrt, atan2, radians

    # radius of earth in km
    r_eq = 6378
    r_poles = 6357
    av_lat = abs((pt1[2] + pt2[1]) / 2)
    r_i = hf.InterpolationValue(r_eq, 0, r_poles, 90, av_lat)

    lat1 = radians(pt1[1])
    lon1 = radians(pt1[0])
    lat2 = radians(pt2[1])
    lon2 = radians(pt2[0])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return r_i * c * 1000



def tif_to_xrds_fixed_lat_lon(tif_path, date, data_name, lats, lons):
    """
    this function converts a tiff to an xarray ds using gdal and numpy
    :param tif_path: a tiff with value 0 for the area that is to be kept and noData for the rest
                    - created in QGIS from original NetCDF that is read into original ds (cells and extent have to perfectly align)
                      by clipping the NetCDF with a shapefile (keep original Extent) and multiply with 0
    :param date: datetime object
    :param nr_rot: number of times the tiff is rotated by 90deg to the right (int)
    :param data_name: the name of the data for the dataset (string)
    :return:
    """
    ds_tif = gdal.Open(tif_path)
    rb = ds_tif.GetRasterBand(1)
    #nodata_val = rb.GetNoDataValue()
    data_arr = np.array(rb.ReadAsArray())
    #data_arr[data_arr == nodata_val] = np.nan

    # make sure the data array has the correct extent - remove the last column and last row
    # because those are outside the array of smaller arrays
    diff_lon = abs(len(data_arr[0]) - len(lons))
    diff_lat = abs(len(data_arr) - len(lats))
    for i in range(diff_lat):
        data_arr = np.delete(data_arr, -1, 0)
    for i in range(diff_lon):
        data_arr = np.delete(data_arr, -1, 1)

    ds = xr.Dataset({data_name: (['time', 'y', 'x'], [data_arr])},  # brackets are for time dimension
                    coords={'x': (['x'], lons),
                            'y': (['y'], lats),
                            'time': pd.date_range(date, periods=1)})
    return ds


def tif_to_xrds(tif_path, date, data_name):
    """
    this function converts a tiff to an xarray ds using gdal and numpy
    :param tif_path: a tiff with value 0 for the area that is to be kept and noData for the rest
                    - created in QGIS from original NetCDF that is read into original ds (cells and extent have to perfectly align)
                      by clipping the NetCDF with a shapefile (keep original Extent) and multiply with 0
    :param date: datetime object
    :param nr_rot: number of times the tiff is rotated by 90deg to the right (int)
    :param data_name: the name of the data for the dataset (string)
    :return:
    """
    ds_tif = gdal.Open(tif_path)
    rb = ds_tif.GetRasterBand(1)
    #nodata_val = rb.GetNoDataValue()
    data_arr = np.array(rb.ReadAsArray())
    #data_arr[data_arr == nodata_val] = np.nan

    #take lats and lons from the tiff
    lons, lats = lat_lon_from_tif(tif_path)

    ds = xr.Dataset({data_name: (['time', 'y', 'x'], [data_arr])},  # brackets are for time dimension
                    coords={'x': (['x'], lons),
                            'y': (['y'], lats),
                            'time': pd.date_range(date, periods=1)})
    return ds

def lat_lon_from_tif(tif_path):
    """
    function returns two lists: the full latitude and full longitude cells (center of cells) of a tiff
    projection has to be EPSG 4326
    :param tif_path: the path of the input tiff
    :return: [latitudes],[longitudes]
    """
    ds = gdal.Open(tif_path)
    a = ds.ReadAsArray()
    nlat, nlon = np.shape(a)

    b = ds.GetGeoTransform()  # bbox, interval
    lons = np.arange(nlon) * b[1] + b[0]
    lats = np.arange(nlat) * b[5] + b[3]
    return lons,lats

def write_NetCDF_to_tiff(nc_file, data_var, x_var_str, y_var_str, EPSG_code, data_type, out_f, mask_tif=""):
    """
    this function writes all arrays that are available at "data_var" to the out_f
    :param nc_file: the nc file path from which all two2d arrays are written to tiff
    :param data_var: the variable string of the netCDF that contains the data (e.g. "ModisFloodWater")
    :param x_var_str: the variable string of the netCDF that contains the x values (usually "lon" or "x")
    :param y_var_str: the variable string of the netCDF that contains the y values (usually "lat" or "y")
    :param EPSG_code: projection code (e.g. 4326)
    :param data_type: 'float32' or 'int16', ....
    :param out_f: the outfolder
    :return:
    """
    ds = xr.open_dataset(nc_file)
    if os.path.exists(mask_tif):
        ds = mask_ds_from_tif(mask_tif, ds, x_var_str, y_var_str, [data_var])
    geotransform, nx, ny = get_geotransform_nx_ny_fromNetCDF(nc_file, x_var_str, y_var_str)
    base_path = os.path.splitext(nc_file)[0]
    nc_file_fn = os.path.split(base_path)[1]
    data_arr = ds[data_var]
    if len(data_arr) > 1:
        for i, ds_slice in enumerate(ds[data_var]):
            np_arr = ds_slice.data
            out_fp = os.path.join(out_f, nc_file_fn+'_'+data_var+str(i+1)+'.tif')
            WriteNumpyToTiff(np_arr, geotransform, ny, nx, EPSG_code, data_type, out_fp)
    elif len(data_arr) == 1:
        np_arr = ds[data_var].data
        out_fp = os.path.join(out_f, nc_file_fn + '_' + data_var + '.tif')
        WriteNumpyToTiff(np_arr, geotransform, ny, nx, EPSG_code, data_type, out_fp)
    else:
        return False

def write_NetCDF_as_mask_to_tiff(nc_file, data_var, x_var_str, y_var_str, mask_value, EPSG_code, data_type, out_fp):
    """
    writes the value passed to the function to a mask
    :param nc_file: the nc file path from which one two2d array is written to tiff
    :param data_var: the variable string of the netCDF that contains the data (e.g. "ModisFloodWater")
    :param x_var_str: the variable string of the netCDF that contains the x values (usually "lon" or "x")
    :param y_var_str: the variable string of the netCDF that contains the y values (usually "lat" or "y")
    :param mask_value: the value that is written to the mask (e.g. 0)
    :param EPSG_code: projection code (e.g. 4326)
    :param data_type: 'float32' or 'int16', ....
    :param out_fp: the outfile path
    :return:s
    """
    ds = xr.open_dataset(nc_file)
    data_arr = ds[data_var]
    if len(data_arr) > 1:
        np_arr = ds[data_var][0].data
    elif len(data_arr) == 1:
        np_arr = ds[data_var].data
    else:
        return False
    np_arr = np.nan_to_num(np_arr)
    np_arr = np_arr*0+1*mask_value
    geotransform, nx, ny = get_geotransform_nx_ny_fromNetCDF(nc_file, x_var_str, y_var_str)
    WriteNumpyToTiff(np_arr, geotransform, ny, nx, EPSG_code, data_type, out_fp)
    return True

