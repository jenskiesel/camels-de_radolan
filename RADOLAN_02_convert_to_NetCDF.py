"""
RADOLAN files are extracted, cut to a catchment and then converted to a NetCDF file

Author: Jens Kiesel
Contact: jenskiesel@gmail.com
Date: June 2021
"""

import datetime
import tarfile

import xarray as xr
import numpy as np
import pandas as pd

import hf_GDAL as ghf
import NetCDF_HelperFunctions as nhf


def get_file_paths_from_folder(folder, ext):
    """
    filter all files within folder with the extension 'ext' and return as list
    :param folder: the 'look-in' folder
    :param ext: extension of file without . dot
    :return: list of files
    """
    p = folder.glob(f'*.{ext}')
    return [x for x in p if x.is_file()]


def extract_tar(fps):
    """
    extracts tar files to the current directory
    :param fp: the input compressed tar files as pathlib.Path list
    """
    for fp in fps:
        dest_fol = fp.parents[0]  # base path
        file = tarfile.open(fp)
        file.extractall(dest_fol)
        file.close()


def move_files(fps, target_f):
    """
    function moves all files to the target folder
    :param fps: all files as pathlib.Path
    :param target_f: the target folder
    """
    for fp in fps:
        fp.rename(target_f / fp.parts[-1])


def get_data_from_gz(fp):
    """
    reads the gz file and returns a dictionary with all subfile names as key
    and values are the file content as a string where lines are separated as '\n'
    :param fp: file path
    :return: the data dictionary
    """
    data = {}
    tar = tarfile.open(fp, "r:gz")
    for member in tar.getmembers():
        f = tar.extractfile(member)
        if f is not None:
            data[member.path] = f.read().decode()
    return {k: data[k] for k in sorted(data)}  # needs to be sorted to maintain the time in increasing order


def get_geoinfo(lines):
    """
    reading the geolocation information from the RADOLAN ASCII file
    :param lines: lines of the ASCII file, already split at '\n'
    :return: the geo information and no data value
    """
    ncols = int(lines[0].split()[1])
    nrows = int(lines[1].split()[1])
    xll = float(lines[2].split()[1])
    yll = float(lines[3].split()[1])
    cellsize = float(lines[4].split()[1])
    no_data = float(lines[5].split()[1])
    return ncols, nrows, xll, yll, cellsize, no_data


def get_datetime_from_asc_name(file_str, force_minutes=False):
    """
    extract the datetime from the radolan file name. A force_minutes variable
    between '00' and '59' can be supplied that re-assigns the minutes to a fixed number
    :param file_str: the file name as string
    :param force_minutes: string between '00' and '59' that replaces the minutes of the radolan time stamp (string)
    :return: datetime object of the current file (radolan time step)
    """
    date_str = file_str.split('_')[1].split('.')[0]
    if isinstance(force_minutes, str):
        date_str = date_str[:-2] + force_minutes
    elif isinstance(force_minutes, float) or isinstance(force_minutes, int):
        raise TypeError(f"Type {type(force_minutes)} not allowed - only 'str' with values from '00 to '59'"
                        f" for force_minutes in RADOLAN time series")
    return datetime.datetime.strptime(date_str, '%Y%m%d-%H%M')


def hourly_radolan_data_to_netcdf(shp_fp, gz_fp, force_minutes, src_proj, src_proj_type, tar_proj4, out_nc_fp):
    """
    function translates the radolan data dictionary (data_dic) which contains
    radolan asci grids and merges all to a NetCDF file and cuts the netcdf to
    the bounding box
    :param shp_fp: the shapefile for which the radolan files will be spatially subset (path)
    :param gz_fp: the radolan compressed filepath with ASCII grids (path)
    :param force_minutes: string between '00' and '59' that replaces the minutes of the radolan time stamp (string)
    :param src_proj: the source projection of the shapefile (int or string)
    :param tar_proj4: the target (radolan) projection (string)
    :param src_proj_type: type of projection 'epsg' or 'proj4' (string)
    :param tar_proj4: radolan proj4 projection definition
    :param out_nc_fp: the output NetCDF
    """
    # get properties of the first file ASSUMING THAT ALL FILES IN ONE gz have the same header
    data_dic = get_data_from_gz(gz_fp)
    hlines = [line for line in data_dic[list(data_dic.keys())[0]].split('\n')[:7]]
    ncols, nrows, xll, yll, cellsize, no_data = get_geoinfo(hlines)
    xll += cellsize/2  # required because the NetCDF uses the center of each cell
    yll += cellsize/2  # not the lower left corner
    x_lst = [xll + cellsize * nrow for nrow in range(nrows)]
    y_lst = [yll + cellsize * ncol for ncol in range(ncols)]

    # get bounding box for subset
    if src_proj is None:
        try:
            if src_proj_type == 'proj4':
                src_proj = ghf.get_proj4_from_shape(shp_fp)
            elif src_proj_type == 'epsg':
                src_proj = ghf.get_epsg_from_shape(shp_fp)
        except:
            raise ValueError(f'Shapefile {shp_fp} must be either projected or projection must be provided')
        if not type(src_proj) is str and not type(src_proj) is int:
            raise ValueError(f'Shapefile {shp_fp} must be either projected or projection must be provided')
    bbx = ghf.get_bbox(shp_fp, cellsize * 2)
    bbx_rad = ghf.reproject_bbox(bbx, src_proj, tar_proj4, 'proj4', 'proj4')

    ds_lst = []
    for key, data in data_dic.items():
        dt = get_datetime_from_asc_name(key, force_minutes)

        # read data and convert to numpy array
        dlines = [line for line in data.split('\n')[6:] if len(line)>0]
        vals = [dline.split() for dline in dlines]
        arr = np.asarray(vals)
        arr = arr.astype(float)
        # need to flip data along y axis: coordinates are calculated from lower left, array is counted from top
        # y axis are the rows of the array, which is axis=0
        arr = np.flip(arr, axis=0)
        arr[arr == no_data] = np.nan

        # add the data and metadata to an xarray dataset and append to list
        ds_raw = xr.Dataset({'precip': (['time', 'y', 'x'], [arr])},  # brackets are for time dimension
                                  coords = {'x': (['x'], x_lst),
                                            'y': (['y'], y_lst),
                                            'time': pd.date_range(dt, periods=1)},
                                  attrs = {'description': 'precipitation in 0.1mm/hr from the DWD RADOLAN dataset',
                                           'projection': 'PROJCS["Stereographic_North_Pole",GEOGCS["GCS_unnamed ellipse",DATUM["D_unknown",SPHEROID["Unknown",6370040,0]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Stereographic_North_Pole"],PARAMETER["standard_parallel_1",60],PARAMETER["central_meridian",10],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]',
                                           'proj4': tar_proj4,
                                           'data_source': r"https://opendata.dwd.de/climate_environment/CDC/grids_germany/hourly/radolan",
                                           'contact': 'Jens Kiesel (jenskiesel@gmail.com)'})
        ds_lst.append(ds_raw)

    # merge all datasets of the gz file to one NetCDF
    ds = xr.concat(ds_lst, dim='time')
    ds = ds.sortby(['time'], ascending=True)

    # subset ds spatially for bounding box of the area of interest
    ds = nhf.spatial_subset_bbox_xy(ds, bbx_rad)
    ds.to_netcdf(out_nc_fp)


def merge_to_one_nc(multiple_ncs_f, out_f_single_nc, out_nc_base_name, out_nc_ext):
    frm_date = list(multiple_ncs_f.glob(f'*{out_nc_ext}'))[0].name.split('.')[0].split('-')[1]
    to_date = list(multiple_ncs_f.glob(f'*{out_nc_ext}'))[-1].name.split('.')[0].split('-')[1]
    out_nc_fp = out_f_single_nc / f'{out_nc_base_name}_{frm_date}-{to_date}{out_nc_ext}'

    fps = get_file_paths_from_folder(multiple_ncs_f, out_nc_ext.replace('.', ''))
    nc_ds = xr.open_mfdataset(fps)
    nc_ds.load().to_netcdf(out_nc_fp)
    return out_nc_fp
