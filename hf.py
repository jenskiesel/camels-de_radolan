

def print_progress_bar(iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end='')  # https://stackoverflow.com/questions/34950201/pycharm-print-end-r-statement-not-working
    # Print New Line on Complete
    if iteration == total:
        print()


def InterpolationValue(x1, y1, x2, y2, xi):
    """
    this function interpolates in between value1 y1 at x1 and value2 y2 at x2 a new value yi at xi
    x1 [float]: "location" of value1 of the interpolation
    x2 [float]: "location" of value2 of the interpolation
    y1 [float]: value1 of the interpolation
    y2 [float]: value2 of the interpolation
    xi [float]: "location" of desired value3
    it returns: the interpolated value yi [float]
    """
    x1 = float(x1)
    x2 = float(x2)
    y1 = float(y1)
    y2 = float(y2)
    xi = float(xi)
    if x1 == x2:
        return False
    #defining linear equation for interpolation
    yi = y1 + ((y2-y1)/(x2-x1))*(xi-x1)
    return yi