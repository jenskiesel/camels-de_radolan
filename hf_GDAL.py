#                   ----------------
#                   HELPER FUNCTIONS
#                   ----------------
#
#
# GDAL must be installed, e.g.
# https://gis.stackexchange.com/questions/44958/gdal-importerror-in-python-on-windows/143140#143140
#
#
import os
import sys
import glob
import copy
import time

try:
    import gdal
    import osr
    import ogr
except ModuleNotFoundError:
    from osgeo import gdal
    from osgeo import osr
    from osgeo import ogr
import osgeo
import numpy as np
import pyproj
from shapely.wkb import loads
from shapely.ops import cascaded_union
from shapely import speedups
speedups.disable()
from osgeo import gdalconst
import netCDF4
from pyproj import Transformer

import ogr2ogr
import NetCDF_HelperFunctions as nchf
import hf as hf


def reproject_geopandas(in_shp, out_shp, tar_proj, proj_type='epsg'):
    import geopandas
    gdf = geopandas.read_file(in_shp)
    if len(gdf) == 0:
        raise ValueError(f"File {in_shp} contains no data or cannot be read by geopandas! "
                         f"Check the file!")
    if proj_type == 'epsg':
        gdf_proj = gdf.to_crs(f"EPSG:{tar_proj}")
    elif proj_type == 'proj4':
        gdf_proj = gdf.to_crs(tar_proj)
    gdf_proj.to_file(out_shp)  # write shp file


def reproject_coordinates(x_lst, y_lst, source, target, src_type='epsg', tar_type='epsg', always_xy=True):
    if src_type == 'epsg':
        inProj = f'epsg:{source}'
    elif src_type == 'proj4':
        inProj = source
    elif src_type == 'wkt':
        inProj = source
    if tar_type == 'epsg':
        outProj = f'epsg:{target}'
    elif tar_type == 'proj4':
        outProj = target
    elif tar_type == 'wkt':
        outProj = target
    xpr_lst = []
    ypr_lst = []
    if len(x_lst) != len(y_lst):
        raise ValueError("Coordinate lists not of equal length")
    for x, y in zip(x_lst, y_lst):
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # !!! EPSG 4326 lat lon is swapped !!!
        # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # https://pyproj4.github.io/pyproj/stable/api/transformer.html#transformer
        transformer = Transformer.from_crs(inProj, outProj, always_xy=always_xy)
        xpr, ypr = transformer.transform(x, y)
        xpr_lst.append(xpr)
        ypr_lst.append(ypr)
    return xpr_lst, ypr_lst


def reproject_point(x, y, source, target, src_type='epsg', tar_type='epsg', always_xy=True):
    if src_type == 'epsg':
        inProj = f'epsg:{source}'
    elif src_type == 'proj4':
        inProj = source
    elif src_type == 'wkt':
        inProj = source
    if tar_type == 'epsg':
        outProj = f'epsg:{target}'
    elif tar_type == 'proj4':
        outProj = target
    elif tar_type == 'wkt':
        outProj = target
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # !!! EPSG 4326 lat lon is swapped !!!
    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    # https://pyproj4.github.io/pyproj/stable/api/transformer.html#transformer
    transformer = Transformer.from_crs(inProj, outProj, always_xy=always_xy)
    return transformer.transform(x, y)


def get_proj4_from_shape(shp_fp):
    shp_prj_fp = os.path.splitext(shp_fp)[0] + '.prj'
    if not os.path.exists(shp_prj_fp):
        raise FileNotFoundError(shp_prj_fp)
    prj_file = open(shp_prj_fp, 'r')
    prj_txt = prj_file.read()
    srs = osr.SpatialReference()
    srs.ImportFromESRI([prj_txt])
    return srs.ExportToProj4()


def get_epsg_from_shape(shp_fp):
    shp_prj_fp = os.path.splitext(shp_fp)[0] + '.prj'
    if not os.path.exists(shp_prj_fp):
        raise FileNotFoundError(shp_prj_fp)
    prj_file = open(shp_prj_fp, 'r')
    prj_txt = prj_file.read()
    srs = osr.SpatialReference()
    srs.ImportFromESRI([prj_txt])
    proj4 = srs.ExportToProj4()
    return proj4_to_epsg(proj4)

def get_epsg_from_prj(prj_fp):
    prj_file = open(prj_fp, 'r')
    prj_txt = prj_file.read()
    srs = osr.SpatialReference()
    srs.ImportFromESRI([prj_txt])
    proj4 = srs.ExportToProj4()
    return proj4_to_epsg(proj4)

def epsg_to_proj4(epsg):
    p2 = pyproj.Proj(f'epsg:{epsg}')
    return p2.srs

def proj4_to_epsg(proj4):
    crs = pyproj.CRS.from_proj4(proj4)
    return crs.to_epsg()

def wkt_to_epsg(wkt):
    crs = pyproj.CRS.from_wkt(wkt)
    return crs.to_epsg

def reproject(in_shp, in_epsg, out_shp, out_epsg):
    """
    does not work - SWITCHES X AND Y COORDINATES!
    """
    driver = ogr.GetDriverByName('ESRI Shapefile')

    # input SpatialReference
    inSpatialRef = osr.SpatialReference()
    inSpatialRef.ImportFromEPSG(in_epsg)

    # output SpatialReference
    outSpatialRef = osr.SpatialReference()
    outSpatialRef.ImportFromEPSG(out_epsg)

    # create the CoordinateTransformation
    coordTrans = osr.CoordinateTransformation(inSpatialRef, outSpatialRef)

    # get the input layer
    inDataSet = driver.Open(in_shp)
    inLayer = inDataSet.GetLayer()

    # create the output layer
    if os.path.exists(out_shp):
        driver.DeleteDataSource(out_shp)
    outDataSet = driver.CreateDataSource(out_shp)
    outLayer = outDataSet.CreateLayer("reproject", geom_type=ogr.wkbMultiPolygon)

    # add fields
    inLayerDefn = inLayer.GetLayerDefn()
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        outLayer.CreateField(fieldDefn)

    # get the output layer's feature definition
    outLayerDefn = outLayer.GetLayerDefn()

    # loop through the input features
    inFeature = inLayer.GetNextFeature()
    while inFeature:
        # get the input geometry
        geom = inFeature.GetGeometryRef()
        # reproject the geometry
        geom.Transform(coordTrans)
        # create a new feature
        outFeature = ogr.Feature(outLayerDefn)
        # set the geometry and attribute
        outFeature.SetGeometry(geom)
        for i in range(0, outLayerDefn.GetFieldCount()):
            outFeature.SetField(outLayerDefn.GetFieldDefn(i).GetNameRef(), inFeature.GetField(i))
        # add the feature to the shapefile
        outLayer.CreateFeature(outFeature)
        # dereference the features and get the next input feature
        outFeature = None
        inFeature = inLayer.GetNextFeature()

    # Save and close the shapefiles
    inDataSet = None
    outDataSet = None


def get_bbox(shp_fp, buffer=0):
    # returns the bounding box of a shapefile with
    # [minx, miny, maxx, maxy]
    VectorFormat = 'ESRI Shapefile'
    VectorDriver = ogr.GetDriverByName(VectorFormat)
    VectorDataset = VectorDriver.Open(str(shp_fp), 0)  # 0=Read-only, 1=Read-Write
    layer = VectorDataset.GetLayer()

    # Iterate through the shapefile features
    bbox = [sys.maxsize, sys.maxsize, -sys.maxsize, -sys.maxsize]  # [minx, miny, maxx, maxy]
    for Count, feature in enumerate(layer):
        sub_box = feature.GetGeometryRef().GetEnvelope()  # minx, maxx, miny, maxy
        bbox = [min(bbox[0], sub_box[0]),
                min(bbox[1], sub_box[2]),
                max(bbox[2], sub_box[1]),
                max(bbox[3], sub_box[3])]
    return extend_bbox(bbox, buffer)


def extend_bbox(bbox, buffer):  # [minx, miny, maxx, maxy]
    bbox[0] = bbox[0] - buffer
    bbox[1] = bbox[1] - buffer
    bbox[2] = bbox[2] + buffer
    bbox[3] = bbox[3] + buffer
    return bbox

def reproject_bbox(bbox, source, target, src_type='epsg', tar_type='epsg', always_xy=True):
    x_lst = [bbox[0], bbox[2]]
    y_lst = [bbox[1], bbox[3]]
    xpr_lst, ypr_lst = reproject_coordinates(x_lst, y_lst, source, target, src_type, tar_type, always_xy)
    return [xpr_lst[0], ypr_lst[0], xpr_lst[1], ypr_lst[1]]


def get_bbox_geopandas(shp_fp):
    import geopandas as gpd
    # returns the bounding box of a shapefile with
    # [minx, miny, maxx, maxy]
    shp_df = gpd.read_file(shp_fp)
    all_bounds = shp_df.bounds
    return [min(all_bounds['minx']), min(all_bounds['miny']), max(all_bounds['maxx']), max(all_bounds['maxy'])]

def bbox_to_shape(bbox, shp_fp, proj, proj_type):
    # Now convert it to a shapefile with OGR
    driver = ogr.GetDriverByName('Esri Shapefile') # reading from ORG
    datasource = driver.CreateDataSource(str(shp_fp))
    #driver = ogr.GetDriverByName('Memory')                                     # For in-memory vector file
    #datasource = driver.CreateDataSource('out')                                # For in-memory vector file
    if datasource is None:
        print("Creation of output file failed.\n")
        raise SystemExit

    # Create the SpatialReference
    coordinateSystem = osr.SpatialReference()

    if proj_type == 'epsg':
        coordinateSystem.ImportFromEPSG(proj)
    elif proj_type == 'proj4':
        coordinateSystem.ImportFromProj4(proj)
    elif proj_type == 'wkt':
        coordinateSystem.ImportFromWkt(proj)

    # Create a new layer on the data source with the indicated name, coordinate system, geometry type
    layer = datasource.CreateLayer(str(shp_fp), coordinateSystem, geom_type=ogr.wkbPolygon)
    if layer is None:
        print("Layer creation failed.\n")
        raise SystemExit

    # Create a new field on a layer. Add one attribute
    layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger64))
    layer.CreateField(ogr.FieldDefn('x_min', ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn('x_max', ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn('y_min', ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn('y_max', ogr.OFTReal))

    # Fetch the schema information for this layer
    LayerDef = layer.GetLayerDefn()

    #create polygon object:
    # [minx, miny, maxx, maxy]
    myRing = ogr.Geometry(type=ogr.wkbLinearRing)
    myRing.AddPoint(bbox[0], bbox[1])
    myRing.AddPoint(bbox[0], bbox[3])
    myRing.AddPoint(bbox[2], bbox[3])
    myRing.AddPoint(bbox[2], bbox[1])
    myRing.AddPoint(bbox[0], bbox[1])                                         #close ring
    geometry = ogr.Geometry(type=ogr.wkbPolygon)
    geometry.AddGeometry(myRing)

    # create point geometry for coordinate system tranformation
    # Set up transformation in case input is not in WGS84
    point_ref=osr.SpatialReference()
    point_ref.ImportFromEPSG(4326)  # WGS84
    if int(osgeo.__version__[0]) >= 3:
        # GDAL 3 changes axis order: https://github.com/OSGeo/gdal/issues/1546
        point_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    ctran=osr.CoordinateTransformation(coordinateSystem, point_ref)          # Create transformation for converting to WGS84
    pt = geometry.Centroid()
    pt.Transform(ctran)

    # Create a new feature (attribute and geometry)
    feature = ogr.Feature(LayerDef)
    feature.SetField('id', 1)
    feature.SetField('x_min',bbox[0])
    feature.SetField('x_max',bbox[2])
    feature.SetField('y_min',bbox[1])
    feature.SetField('y_max',bbox[3])

    # Make a geometry from Shapely object
    feature.SetGeometry(geometry)
    layer.CreateFeature(feature)


def project_to_input(infeatures, outputFile, to_project):
    '''This function projects the input features (to_project) in ESRI Shapefile
    format and uses ogr2ogr.py (if present in the same directory as this script)
    to reproject 'to_project' shapefile to the coordinate system of 'infeatures'
    shapefile.'''

    driver = ogr.GetDriverByName('ESRI Shapefile')
    shp = driver.Open(infeatures, 0)                                            # Opening the file with GDAL, with read only acces
    lyr = shp.GetLayer()
    spatialref = lyr.GetSpatialRef().ExportToWkt()
    if shp is None:
        print("Open failed.\n")
    ogr2ogr.main(["","-f", "ESRI Shapefile", "-t_srs", spatialref, outputFile, to_project])   #note: main is expecting sys.argv, where the first argument is the script name, so the argument indices in the array need to be offset by 1
    shp = lyr = spatialref = None
    return outputFile


def check_prj_cellsize_aligned(x_min1, x_min2, x_max1, x_max2, y_min1, y_min2, y_max1, y_max2,
                               cell_size1, cell_size2, epsg1, epsg2, alignment_acc):
    """
    checking if two rasters have the same projection, cellsize and if cells are aligned
    with alignemnt_acc (raster units)
    """
    check = True
    if epsg1 != epsg2:
        print(f"WARNING: EPSG Codes {epsg1} and {epsg2} do not match")
    if cell_size1 != cell_size2:
        print(f"Cellsizes {cell_size1} and {cell_size2} do not match")
        check = False
    x_diff_1 = abs((x_min2 - x_min1) / cell_size1)
    x_diff_2 = abs((x_max2 - x_max1) / cell_size1)
    y_diff_1 = abs((y_min2 - y_min1) / cell_size1)
    y_diff_2 = abs((y_max2 - y_max1) / cell_size1)
    max_diff = max([abs(int(x_diff_1) - x_diff_1),
                    abs(int(x_diff_2) - x_diff_2),
                    abs(int(y_diff_1) - y_diff_1),
                    abs(int(y_diff_2) - y_diff_2)])
    if max_diff > alignment_acc:
        print(f"WRONG ALIGNMENT of rasters")
        check = False
    return check


def check_prj_cellsize_aligned(x_min1, x_min2, x_max1, x_max2, y_min1, y_min2, y_max1, y_max2,
                               cell_size1, cell_size2, epsg1, epsg2, alignment_acc):
    """
    checking if two rasters have the same projection, cellsize and if cells are aligned
    with alignemnt_acc (raster units)
    """
    check = True
    if epsg1 != epsg2:
        print(f"WARNING: EPSG Codes {epsg1} and {epsg2} do not match")
    if cell_size1 != cell_size2:
        print(f"Cellsizes {cell_size1} and {cell_size2} do not match")
        check = False
    x_diff_1 = abs((x_min2 - x_min1) / cell_size1)
    x_diff_2 = abs((x_max2 - x_max1) / cell_size1)
    y_diff_1 = abs((y_min2 - y_min1) / cell_size1)
    y_diff_2 = abs((y_max2 - y_max1) / cell_size1)
    max_diff = max([abs(int(x_diff_1) - x_diff_1),
                    abs(int(x_diff_2) - x_diff_2),
                    abs(int(y_diff_1) - y_diff_1),
                    abs(int(y_diff_2) - y_diff_2)])
    if max_diff > alignment_acc:
        print(f"WRONG ALIGNMENT of rasters")
        check = False
    return check

def get_array_for_extent(fn, offset_and_size):
    # Get the data for an optional offset and size
    # if offset and size = None, whole area is returned
    ds = gdal.Open(fn)
    rb = ds.GetRasterBand(1)
    if offset_and_size is None:
        np_array = np.array(rb.ReadAsArray())
    else:
        np_array = np.array(rb.ReadAsArray(xoff=offset_and_size[0],
                                           yoff=offset_and_size[1],
                                           win_xsize=offset_and_size[2],
                                           win_ysize=offset_and_size[3]))
    no_data = rb.GetNoDataValue()
    masked_data = np.ma.masked_where(np_array == no_data, np_array)
    masked_data[masked_data.mask] = np.nan  # set mask to no data
    return masked_data, no_data

def get_xoff_yoff_winxsize_winysize(ext, overl_ext, cellsize):
    '''
    function returns the offset in number of columns (x)
    and number of rows (y) as well as number of cols (win_xsize)
    and rows (win_ysize) from the ext to the overlap extent
    overl_ext must be part (spatially) of ext
    '''
    xoff = int((max(ext[0], overl_ext[0]) - \
                min(ext[0], overl_ext[0])) / cellsize)
    yoff = int((max(ext[1], overl_ext[1]) - \
                min(ext[1], overl_ext[1])) / cellsize)
    win_xsize = int((overl_ext[2] - overl_ext[0]) / cellsize)
    win_ysize = int((overl_ext[1] - overl_ext[3]) / cellsize)
    return (xoff, yoff, win_xsize, win_ysize)


def get_overlapping_arrays(fn1, fn2):
    """
    this function extract the overlapping part of two rasters to numpy arrays
    IMPORTANT: Both rasters must have the same projection, cellsize and cells must be aligned.
    They can have different extents!
    :param fn1: path to raster 1
    :param fn2: path to raster 2
    :return: arr1 with properties, arr2 with properties in two lists
    """
    alignment_acc = 0.0001
    geotransform1, cell_size1, x_min1, x_max1, nx1, y_min1, y_max1, ny1, srs1, epsg1 = get_geo_transform_properties(fn1)
    geotransform2, cell_size2, x_min2, x_max2, nx2, y_min2, y_max2, ny2, srs2, epsg2 = get_geo_transform_properties(fn2)
    if check_prj_cellsize_aligned(x_min1, x_min2, x_max1, x_max2, y_min1, y_min2, y_max1, y_max2,
                                  cell_size1, cell_size2, epsg1, epsg2, alignment_acc):
        # cellsize is equal - so just using cell_size1 for now
        # returns [min_x, max_y, max_x, min_y]
        extent1 = [x_min1, y_max1, x_max1, y_min1]
        extent2 = [x_min2, y_max2, x_max2, y_min2]
        overlap_extent = [max(x_min1, x_min2), min(y_max1, y_max2),
                          min(x_max1, x_max2), max(y_min1, y_min2)]

        offset_and_size1 = get_xoff_yoff_winxsize_winysize(extent1, overlap_extent, cell_size1)
        offset_and_size2 = get_xoff_yoff_winxsize_winysize(extent2, overlap_extent, cell_size1)

        arr1 = get_array_for_extent(fn1, offset_and_size1)[0]
        arr2 = get_array_for_extent(fn2, offset_and_size2)[0]

        # define output grid properties
        nx1, ny1 = (offset_and_size1[2], offset_and_size1[3])
        nx2, ny2 = (offset_and_size2[2], offset_and_size2[3])
        if not nx1 == nx2:
            print(f"arrays do not have equal x coordinates")
        if not ny1 == ny2:
            print(f"arrays do not have equal y coordinates")

        gt = (overlap_extent[0], cell_size1, 0.0, overlap_extent[1], 0.0, -cell_size1)

        return (arr1, arr2, gt, nx1, ny1)


def clip_and_align_tif(InputImage, Shapefile, out_folder):
    import gdal, ogr
    RasterFormat = 'GTiff'
    VectorFormat = 'ESRI Shapefile'

    # Open datasets
    Raster = gdal.Open(InputImage, gdal.GA_ReadOnly)
    Projection = Raster.GetProjectionRef()
    geo_transform = Raster.GetGeoTransform()
    cell_size = geo_transform[1]
    VectorDriver = ogr.GetDriverByName(VectorFormat)
    VectorDataset = VectorDriver.Open(Shapefile, 0)  # 0=Read-only, 1=Read-Write
    layer = VectorDataset.GetLayer()
    FeatureCount = layer.GetFeatureCount()

    # Iterate through the shapefile features
    for Count, feature in enumerate(layer):
        geom = feature.GetGeometryRef()
        minX, maxX, minY, maxY = geom.GetEnvelope()  # Get bounding box of the shapefile feature
        # Create raster
        if Count > 0:
            OutTileName = os.path.join(out_folder, str(Count) + os.path.split(InputImage)[1])
        else:
            OutTileName = os.path.join(out_folder, os.path.split(InputImage)[1])
        OutTile = gdal.Warp(OutTileName, Raster, format=RasterFormat, outputBounds=[minX, minY, maxX, maxY],
                            xRes=cell_size, yRes=cell_size, dstSRS=Projection, resampleAlg=gdal.GRA_NearestNeighbour,
                            options=['COMPRESS=DEFLATE'])
        OutTile = None  # Close dataset
    Raster = None
    VectorDataset.Destroy()


def check_no_data(no_data_1, no_data_2, fp1, fp2):
    if no_data_1 != no_data_2:
        print(f"No data value 1 '{no_data_1}' from '{fp1}' not equal to "
              f"no data value 2 '{no_data_2}' from '{fp2}'!")

def get_geo_transform_properties(raster_file):
    """
    returns preoperties from the geo transform
    :param geo_transform: the geo transform object
    :param return_property: x_min, x_max, y_min, y_max, x_res, y_res, pixel_width,
    :return:
    """
    data = gdal.Open(raster_file, gdalconst.GA_ReadOnly)
    geo_transform = data.GetGeoTransform()
    x_min = geo_transform[0]
    y_max = geo_transform[3]
    x_max = x_min + geo_transform[1] * data.RasterXSize
    y_min = y_max + geo_transform[5] * data.RasterYSize
    nx = data.RasterXSize
    ny = data.RasterYSize
    cell_size = geo_transform[1]
    srs = osr.SpatialReference(wkt=data.GetProjection())
    epsg = srs.GetAttrValue('AUTHORITY', 1)
    return geo_transform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg


def divide_tiffs(dividend_fp, divisor_fp, quotient_fp):
    """
    this function divides the values of the tiffs - if division by 0, 0 is returned
    :param dividend_fp:
    :param divisor_fp:
    :param quotient_fp:
    :return:
    """
    geotransform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(dividend_fp)
    dividend_arr, no_data_1 = ReadSingleBandRasterToArray(dividend_fp)
    divisor_arr, no_data_2 = ReadSingleBandRasterToArray(divisor_fp)
    check_no_data(no_data_1, no_data_2, dividend_fp, divisor_fp)
    dividend_arr = np.where(dividend_arr == no_data_1, np.nan, dividend_arr)
    divisor_arr = np.where(divisor_arr == no_data_2, np.nan, divisor_arr)
    quotient_arr = np.true_divide(dividend_arr, divisor_arr)
    #quotient_arr[quotient_arr == np.inf] = 0
    #quotient_arr = np.nan_to_num(quotient_arr)
    WriteNumpyToTiff(quotient_arr, geotransform, nx, ny, srs, str(quotient_arr.dtype), quotient_fp, no_data_1)

def multiply_tiffs(factors_lst_fps, product_fp):
    """
    this function multiplies the values of the tiffs
    :param factors_lst_fps: a list with tiffs to be multiplied with each other
    :param product_fp: the resulting file path
    :return:
    """
    geotransform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(factors_lst_fps[1])
    product_arr, no_data_1 = ReadSingleBandRasterToArray(factors_lst_fps[0])
    for next_factor_fp in factors_lst_fps[1:]:
        next_factor_arr, no_data_2 = ReadSingleBandRasterToArray(next_factor_fp)
        check_no_data(no_data_1, no_data_2, factors_lst_fps[0], next_factor_fp)
        product_arr = np.multiply(product_arr, next_factor_arr)
    WriteNumpyToTiff(product_arr, geotransform, nx, ny, srs, str(product_arr.dtype), product_fp, no_data_1)


def merge_tiffs(tiff_1_fp, tiff_2_fp, out_fp):
    """
    function merges two tiffs with the following conditions:
    - tiff_1 values overwrite tiff 2 values
    - tiff_2 values overwrite no_data values of tiff_1
    - cells with no_data values at tiff_1 and tiff_2 remain no_data
    :param tiff_1_fp:
    :param tiff_2_fp:
    :return:
    """
    geotransform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(tiff_1_fp)
    arr_1, no_data_1 = ReadSingleBandRasterToArray(tiff_1_fp)
    arr_2, no_data_2 = ReadSingleBandRasterToArray(tiff_2_fp)
    #where arr_1 == no data, take values of tiff_2
    out_arr = np.where(np.isnan(arr_1), arr_2.data, arr_1.data)
    WriteNumpyToTiff(out_arr, geotransform, nx, ny, srs, str(out_arr.dtype), out_fp, no_data_1)


def GetRasterValue(x,y,ras_path):
    #
    # this function returns the raster value
    # at location x/y in the coordinate system
    # of the raster
	#
	# # 0.005s for the lines until x_loc and y_loc
	#
	# https://gis.stackexchange.com/questions/46893/getting-pixel-value-of-gdal-raster-under-ogr-point-without-numpy
    # http://geoexamples.blogspot.de/2012/02/raster-classification-with-gdal-python.html
	# https://gis.stackexchange.com/questions/73191/python-gdal-best-way-to-read-in-raster-for-use-in-a-search-algortihm
    #
    import gdal
    import struct
    src_ds = gdal.Open(ras_path)
    gt = src_ds.GetGeoTransform()
    rb = src_ds.GetRasterBand(1)
    data_type_id = rb.DataType
    raster_type = gdal.GetDataTypeName(data_type_id)
    data_types = {'Byte': 'B', 'UInt16': 'H', 'Int16': 'h', 'UInt32': 'I', 'Int32': 'i', 'Float32': 'f', 'Float64': 'd'}
    unstruct_str = data_types[raster_type]
    ncols = src_ds.RasterXSize
    nrows = src_ds.RasterYSize
    xllcorner = gt[0]
    yllcorner = gt[3] - gt[1] * src_ds.RasterYSize
    cellsize = gt[1]
    xloc = int((x - xllcorner) / cellsize)  # the location (column number) of x from the left edge
    yloc = int((nrows - (y - yllcorner) / cellsize))  # the location (line number) of y from the upper edge
    structval = rb.ReadRaster(xloc, yloc, 1, 1, buf_type=data_type_id)
    val = struct.unpack(unstruct_str, structval)
    return val[0]


def gaussian_blur(in_array, size):
    #
    # Smoothing of raster, Answer by Mike T:
    # https://gis.stackexchange.com/questions/9431/what-raster-smoothing-generalization-tools-are-available
    #
    from scipy.signal import fftconvolve
    # expand in_array to fit edge of kernel
    padded_array = np.pad(in_array, size, 'symmetric')
    # build kernel
    x, y = np.mgrid[-size:size + 1, -size:size + 1]
    g = np.exp(-(x ** 2 / float(size) + y ** 2 / float(size)))
    g = (g / g.sum()).astype(in_array.dtype)
    # do the Gaussian blur
    return fftconvolve(padded_array, g, mode='valid')


def ReadSingleBandRasterToArray(ras_path):
    #
    # this function reads a raster file to a numpy array
    #
    ds = gdal.Open(ras_path)
    rb = ds.GetRasterBand(1)
    np_array = np.array(rb.ReadAsArray())
    no_data = rb.GetNoDataValue()
    np_array = np.where(np_array == no_data, np.NaN, np_array)
    masked_data = np.ma.masked_where(np_array == no_data, np_array)
    try:
        masked_data[masked_data.mask] = np.nan  # set mask to no data
    except ValueError:  # then input raster is likely int
        pass
    return masked_data, no_data



def WriteNumpyToTiff(np_array, geotransform, nx, ny, srs, data_type, outfile, no_data):
    #
    # GDAL Types
    # https://gist.github.com/CMCDragonkai/ac6289fa84bcc8888035744d7e00e2e6
    #
    dst_ds = create_GDAL_dataset('GTiff', outfile, nx, ny, 1, data_type)
    dst_ds.SetGeoTransform(geotransform)    # specify coords
    dst_ds.SetProjection(srs.ExportToWkt()) # export coords to file
    rb = dst_ds.GetRasterBand(1)
    rb.WriteArray(np_array)   # write band to the raster
    rb.SetNoDataValue(no_data)
    dst_ds.FlushCache()                     # write to disk


def create_GDAL_dataset(driver, outfile, nx, ny, bands, data_type):
    """
    creates a GDAL dataset
    :param driver: e.g. 'GTiff'
    :param outfile: path where the file is saved
    :param nx: number of values in x direction
    :param ny: number of values in y direction
    :param bands: usually 1
    :param data_type: string, see below
    :return:
    """
    if data_type == "float32" or data_type == "Float32":
        dst_ds = gdal.GetDriverByName(driver).Create(outfile, nx, ny, bands, gdal.GDT_Float32)
    elif data_type == "float64" or data_type == "Float64":
        dst_ds = gdal.GetDriverByName(driver).Create(outfile, nx, ny, bands, gdal.GDT_Float64)
    elif data_type == "uint8" or data_type == "uint16":
        dst_ds = gdal.GetDriverByName(driver).Create(outfile, nx, ny, bands, gdal.GDT_UInt16)
    elif data_type == "int16" or data_type == "Int16":
        dst_ds = gdal.GetDriverByName(driver).Create(outfile, nx, ny, bands, gdal.GDT_Int16)
    elif data_type == "int32" or data_type == "Int32":
        dst_ds = gdal.GetDriverByName(driver).Create(outfile, nx, ny, bands, gdal.GDT_Int32)
    else:
        print("Data type '%s' not supported - add type to function 'WriteNumpyToTiff'"%(data_type))
    return dst_ds


def all_features_to_single_rasters(shape_file, name_field, value_field, raster_file,
                                   no_data, data_type, outfolder, burn_in_value = None):
    """
    function converts all polygons of the shapefile to single rasters (only Byte - for float, ints etc change below)
    where the extent and cellsize is taken from the raster_file
    this is basically a clip of the raster using the shapefile!
    :param shape_file: input shapefile
    :param name_field: string of the field of the shapefile that is used to define the tiff outfile name
    :param value_field: string of the field of the shapefile that is used to define the tiff value
    :param raster_file: input raster file
    :param outfolder: output folder where all rasters are saved
    :param burn_in_value: if a number is passed to it, the value is burned into the tiff regardless of the value_field
    :return:
    """
    shp_file_name = os.path.split(os.path.splitext(shape_file)[0])[1]
    geo_transform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(raster_file)

    # open shapefile and create layer
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shape_file, 0)
    layer = dataSource.GetLayer()
    nr_features = layer.GetFeatureCount()

    out_fps = []
    for feat_id in range(nr_features):
        feature = layer.GetFeature(feat_id)
        if not burn_in_value is None:
            value = burn_in_value
        else:
            value = feature.GetField(value_field)
        name = feature.GetField(name_field)
        output = os.path.join(outfolder, "ras_"+ shp_file_name +'_'+str(name)+'_'+ str(feat_id)+".tif")
        out_fps.append(output)

        # Create a memory layer
        outDriver = ogr.GetDriverByName('MEMORY')
        outDataSource = outDriver.CreateDataSource('memData')
        outLayer = outDataSource.CreateLayer("memLayer", srs, ogr.wkbPolygon)

        # Add field to the layer
        field_obj = ogr.FieldDefn(value_field, ogr.OFTInteger)
        outLayer.CreateField(field_obj)

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        feat = ogr.Feature(featureDefn)
        geom = feature.GetGeometryRef()

        feat.SetGeometry(geom)
        feat.SetField(value_field, value)
        outLayer.CreateFeature(feat)

        target_ds = create_GDAL_dataset('GTiff', output, nx, ny, 1, data_type)
        target_ds.SetGeoTransform(geo_transform)
        target_ds.SetProjection(srs.ExportToWkt())  # export coords to file
        rb = target_ds.GetRasterBand(1)
        rb.SetNoDataValue(no_data)
        gdal.RasterizeLayer(target_ds, [1], outLayer, options=["ATTRIBUTE="+value_field])

        # clean up
        target_ds = None
    return out_fps

def all_features_to_single_rasters_two_fields_merge(shape_file, name_field_1, name_field_2, value_field, raster_file,
                                                    no_data, data_type, outfolder, burn_in_value = None):
    """
    function converts all polygons of the shapefile to single rasters where the extent, cellsize
    is taken from the raster_file - creates a temp file which is deleted
    :param shape_file: input shapefile
    :param name_field1: string of the field of the shapefile that is used to define the tiff outfile name
    :param name_field2: string of the field of the shapefile that is used to define the tiff outfile name
    :param value_field: string of the field of the shapefile that is used to define the tiff value
    :param raster_file: input raster file
    :param outfolder: output folder where all rasters are saved
    :param burn_in_value: if a number is passed to it, the value is burned into the tiff regardless of the value_field
    :return:
    """
    shp_file_name = os.path.split(os.path.splitext(shape_file)[0])[1]
    geo_transform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(raster_file)

    # open shapefile and create layer
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shape_file, 0)
    layer = dataSource.GetLayer()
    nr_features = layer.GetFeatureCount()

    for feat_id in range(nr_features):
        feature = layer.GetFeature(feat_id)
        if not burn_in_value is None:
            value = burn_in_value
        else:
            value = feature.GetField(value_field)
        name_1 = feature.GetField(name_field_1)
        name_2 = feature.GetField(name_field_2)[0:2]

        # Create a memory layer
        outDriver = ogr.GetDriverByName('MEMORY')
        outDataSource = outDriver.CreateDataSource('memData')
        outLayer = outDataSource.CreateLayer("memLayer", srs, ogr.wkbPolygon)

        # Add field to the layer
        field_obj = ogr.FieldDefn(value_field, ogr.OFTInteger)
        outLayer.CreateField(field_obj)

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        feat = ogr.Feature(featureDefn)
        geom = feature.GetGeometryRef()

        feat.SetGeometry(geom)
        feat.SetField(value_field, value)
        outLayer.CreateFeature(feat)

        tmp_fp = r'/vsimem/example.tif'  # in memory: https://github.com/mapbox/rasterio/issues/899
        target_ds = create_GDAL_dataset('GTiff', tmp_fp, nx, ny, 1, data_type)

        target_ds.SetGeoTransform(geo_transform)
        target_ds.SetProjection(srs.ExportToWkt())  # export coords to file
        target_ds.GetRasterBand(1).SetNoDataValue(no_data)
        gdal.RasterizeLayer(target_ds, [1], outLayer, options=["ATTRIBUTE="+value_field])
        target_ds = None  # REQUIRED!!

        out_fp = os.path.join(outfolder, "ras_" + shp_file_name + '_' + name_1 + '_' + name_2 + '_' + str(feat_id) + ".tif")
        merge_tiffs(tmp_fp, raster_file, out_fp)  # handle extent and no data



def all_features_to_raster(shp_fp, value_field, mask_fp, out_data_type, out_fp):
    """
    converts all features of a shapefile to raster. Features may not overlap
    and value_field has to be numeric
    :param shp_fp: the input shapefile
    :param value_field: field name with numeric value to burn into grid
    :param mask_fp: raster mask file path (defines extent, nodata regions and resolution)
    :param out_data_type: data type of output raster
    :param out_fp: raster output file path
    :return:
    """
    geo_transform, cell_size, x_min, x_max, nx, y_min, y_max, ny, srs, epsg = get_geo_transform_properties(mask_fp)
    source_arr, no_data = ReadSingleBandRasterToArray(mask_fp)
    out_arr = copy.deepcopy(source_arr)
    # open shapefile and create layer
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shp_fp, 0)
    layer = dataSource.GetLayer()
    nr_features = layer.GetFeatureCount()

    for feat_id in range(nr_features):
        feature = layer.GetFeature(feat_id)
        value = feature.GetField(value_field)

        # Create a memory layer
        outDriver = ogr.GetDriverByName('MEMORY')
        outDataSource = outDriver.CreateDataSource('memData')
        outLayer = outDataSource.CreateLayer("memLayer", srs, ogr.wkbPolygon)

        # Add field to the layer
        field_obj = ogr.FieldDefn(value_field, ogr.OFTInteger)
        outLayer.CreateField(field_obj)

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        feat = ogr.Feature(featureDefn)
        geom = feature.GetGeometryRef()

        feat.SetGeometry(geom)
        feat.SetField(value_field, value)
        outLayer.CreateFeature(feat)

        tmp_fp = r'/vsimem/example.tif'  # in memory: https://github.com/mapbox/rasterio/issues/899
        target_ds = create_GDAL_dataset('GTiff', tmp_fp, nx, ny, 1, 'Float32')  # needs to be float to handle np.nan
        target_ds.SetGeoTransform(geo_transform)
        target_ds.SetProjection(srs.ExportToWkt())  # export coords to file
        target_ds.GetRasterBand(1).SetNoDataValue(no_data)
        gdal.RasterizeLayer(target_ds, [1], outLayer, options=["ATTRIBUTE="+value_field])
        target_ds = None  # REQUIRED AT THIS LOCATION!!
        arr, nd = ReadSingleBandRasterToArray(tmp_fp)
        arr = np.where(np.isnan(arr), 0.0, arr.data)
        #arr2 = np.where(np.isnan(arr), source_arr.data, arr.data)
        out_arr = out_arr + arr  # add up all single rasterized files
    nchf.WriteNumpyToTiff(out_arr, geo_transform, nx, ny, srs, out_data_type, out_fp, no_data)


def shapefile_features_to_single_features(shape_file, outfolder):
    from osgeo import ogr
    import os

    # open shapefile and create layer
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shape_file, 0)
    layer = dataSource.GetLayer()
    nr_features = layer.GetFeatureCount()

    for feat_id in range(nr_features):
        feature = layer.GetFeature(feat_id)
        id_no = feature.GetField("id_no")
        presence = feature.GetField("presence")
        outShapefile = os.path.join(outfolder, "feature" + str(feat_id) + ".shp")
        outDriver = ogr.GetDriverByName("ESRI Shapefile")

        # Remove output shapefile if it already exists
        if os.path.exists(outShapefile):
            outDriver.DeleteDataSource(outShapefile)

        # Create the output shapefile
        outDataSource = outDriver.CreateDataSource(outShapefile)
        outLayer = outDataSource.CreateLayer("feature" + str(feat_id), geom_type=ogr.wkbPolygon)

        # Add an ID field
        idField = ogr.FieldDefn("id_no", ogr.OFTInteger)
        outLayer.CreateField(idField)
        # Add a presence field
        presField = ogr.FieldDefn("presence", ogr.OFTInteger)
        outLayer.CreateField(presField)
        # add other fields....

        # Create the feature and set values
        featureDefn = outLayer.GetLayerDefn()
        feat = ogr.Feature(featureDefn)
        geom = feature.GetGeometryRef()

        feat.SetGeometry(geom)
        feat.SetField("id_no", id_no)
        feat.SetField("presence", presence)
        outLayer.CreateFeature(feat)
        feat = None
        target_ds = None
        new_dataSource = None

    dataSource = None


def polygonize_raster(inraster, outputFile, valfield=True):
    '''
    Function courtesy of NCAR, supplied by Andrew Newman (April 2021), written by
    Kevin Sampson
    Associate Scientist
    National Center for Atmospheric Research
    ksampson@ucar.edu
    Function takes a directory and a raster (as defined by GDAL) and creates
    a polygon shapefile with one feature per raster cell.'''

    gdal.AllRegister()

    # Opening the file with GDAL, with read only acces
    dataset = gdal.Open(str(inraster), gdal.GA_ReadOnly)

    # Getting raster dataset information
    print('Input Raster Size: %s x %s x %s' %(dataset.RasterXSize,dataset.RasterYSize,dataset.RasterCount))
    print('Projection of input raster: %s' %dataset.GetProjection())
    geotransform = dataset.GetGeoTransform()
    x0 = geotransform[0] # top left x
    y0 = geotransform[3] # top left y
    pwidth = geotransform[1] # pixel width
    pheight = geotransform[5] # pixel height is negative because it's measured from top
    if not geotransform is None:
        print('Origin (x,y): %s,%s' %(x0,y0))
        print('Pixel Size (x,y): %s,%s' %(pwidth,pheight))

    # Get raster values as array (must have Numpy 1.8.1 or greater)
    band = dataset.GetRasterBand(1)
    try:
        ndv = float(band.GetNoDataValue())
    except TypeError:
        print(f"No data value not defined, falling back to -9999")
        ndv = -9999
    DataType = band.DataType
    print('NoData Value: %s' %ndv)
    print('Raster DataType: %s' %gdal.GetDataTypeName(DataType))
    #data = band.ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize)
    data = band.ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize).astype(np.float)

    # Now convert it to a shapefile with OGR
    driver = ogr.GetDriverByName('Esri Shapefile') # reading from ORG
    datasource = driver.CreateDataSource(str(outputFile))
    #driver = ogr.GetDriverByName('Memory')                                     # For in-memory vector file
    #datasource = driver.CreateDataSource('out')                                # For in-memory vector file
    if datasource is None:
        print("Creation of output file failed.\n")
        raise SystemExit

    # Create the SpatialReference
    coordinateSystem = osr.SpatialReference()
    coordinateSystem.ImportFromWkt(dataset.GetProjection())                     # Use projection from input raster
    #src_epsg = pyproj.CRS.from_wkt(dataset.GetProjection()).to_epsg()
    src_wkt = dataset.GetProjection()

    # Create a new layer on the data source with the indicated name, coordinate system, geometry type
    layer = datasource.CreateLayer(str(outputFile), coordinateSystem, geom_type=ogr.wkbPolygon)
    if layer is None:
        print("Layer creation failed.\n")
        raise SystemExit

    # Create a new field on a layer. Add one attribute
    layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger64))
    layer.CreateField(ogr.FieldDefn('i_index', ogr.OFTInteger))
    layer.CreateField(ogr.FieldDefn('j_index', ogr.OFTInteger))
    layer.CreateField(ogr.FieldDefn('lon_cen', ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn('lat_cen', ogr.OFTReal))
    layer.CreateField(ogr.FieldDefn('CELLVALUE', ogr.OFTReal))

    # Fetch the schema information for this layer
    LayerDef = layer.GetLayerDefn()

    # Set up transformation in case input is not in WGS84
    # THIS DOES NOT WORK IF INPUT EPSG IS 3034 !!!!!
    """
    point_ref=osr.SpatialReference()
    point_ref.ImportFromEPSG(4326)  # WGS84
    if int(osgeo.__version__[0]) >= 3:
        # GDAL 3 changes axis order: https://github.com/OSGeo/gdal/issues/1546
        point_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    ctran=osr.CoordinateTransformation(coordinateSystem, point_ref)          # Create transformation for converting to WGS84
    """

    # For each cell, create a rectangle
    i = 1
    counter = 0
    for x in range(dataset.RasterXSize):
        hf.print_progress_bar(x, dataset.RasterXSize)
        j = 1
        for y in range(dataset.RasterYSize):

            # Control flow in order to avoid creating polygons where pixel value is nodata
            if valfield == True and data[y,x] == ndv:
                j += 1
                continue

            # Calculating the polygon's coordinates that frame the raster image
            x00 = x0 + (pwidth * x)
            y00 = y0 - (abs(pheight) * y)
            x1 = x00 + pwidth
            y1 = y00
            x2 = x00
            y2 = y00 - abs(pheight)
            x3 = x1
            y3 = y2

            #create polygon object:
            myRing = ogr.Geometry(type=ogr.wkbLinearRing)
            myRing.AddPoint(x00, y00)
            myRing.AddPoint(x1, y1)
            myRing.AddPoint(x3, y3)
            myRing.AddPoint(x2, y2)
            myRing.AddPoint(x00, y00)                                           #close ring
            geometry = ogr.Geometry(type=ogr.wkbPolygon)
            geometry.AddGeometry(myRing)

            # create point geometry for coordinate system transformation
            pt = geometry.Centroid()
            lon, lat = reproject_point(pt.GetX(), pt.GetY(), src_wkt, 4326, src_type='wkt', tar_type='epsg', always_xy=True)
            # pt.Transform(ctran)

            # Create a new feature (attribute and geometry)
            feature = ogr.Feature(LayerDef)
            feature.SetField('id', counter)
            feature.SetField('i_index', i)
            feature.SetField('j_index', j)
            #feature.SetField('lon_cen', pt.GetX())
            #feature.SetField('lat_cen', pt.GetY())
            feature.SetField('lon_cen', lon)
            feature.SetField('lat_cen', lat)
            feature.SetField('CELLVALUE', data[y,x])                       # Add in the raster value

            # Make a geometry from Shapely object
            feature.SetGeometry(geometry)
            layer.CreateFeature(feature)

            #flush memory
            feature = geometry = myRing = pt = None  # destroy these
            counter += 1
            j += 1
        i += 1

    # Save and close everything
    datasource = layer = None
    return outputFile

def checkfield(layer, fieldname, string1):
    # Check for existence of provided fieldnames
    layerDefinition = layer.GetLayerDefn()
    fieldslist = []
    for i in range(layerDefinition.GetFieldCount()):
        fieldslist.append(layerDefinition.GetFieldDefn(i).GetName())
    if fieldname in fieldslist:
        i = fieldslist.index(fieldname)
        field_defn = layerDefinition.GetFieldDefn(i)
    else:
        print(" Field %s not found in input %s. Terminating..." %(fieldname, string1))
        raise SystemExit
    return field_defn, fieldslist

def getfieldinfo(field_defn, fieldname):
    # Get information about field type for building the output NetCDF file later
    if field_defn.GetType() == ogr.OFTInteger:
        fieldtype = 'integer'                                                   #print "%d" % feat.GetFieldAsInteger(i)
    elif field_defn.GetType() == ogr.OFTReal:
        fieldtype = 'real'                                                      #print "%.3f" % feat.GetFieldAsDouble(i)
#        print "field type: OFTReal not currently supported in output NetCDF file."
    elif field_defn.GetType() == ogr.OFTInteger64:
        fieldtype = 'integer64'                                                 #print "%d" % feat.GetFieldAsInteger(i)
    elif field_defn.GetType() == ogr.OFTString:
        fieldtype = 'string'                                                    #print "%s" % feat.GetFieldAsString(i)
    else:
        fieldtype = 'string'                                                    #print "%s" % feat.GetFieldAsString(i)
    print("    Field Type for field '%s': %s (%s)" %(fieldname, field_defn.GetType(), fieldtype))
    return fieldtype

def spatial_weights(inshp1, inshp2, fieldname1, fieldname2, gridflag=0):
    '''
    Function courtesy of NCAR, supplied by Andrew Newman (April 2021), written by
    Kevin Sampson
    Associate Scientist
    National Center for Atmospheric Research
    ksampson@ucar.edu
    This function takes two input shapefiles (both must be in the same coordinate
    reference sysetm) and performs polygon-by-polygon analysis to compute the
    individual weight of each grid cell using only OGR routines.  If you wish to
    use a fieldname with non-unique values, then it should be the first input
    (inshp1 and fieldname1), and not the second.'''

    tic = time.time()

    # Initiate dictionaries
    spatialweights = {}

    # Open the basin shapefile file with OGR, with read only access
    driver = ogr.GetDriverByName('ESRI Shapefile')
    shp1 = driver.Open(str(inshp1), 0)  # 0 means read-only. 1 means writeable.
    if shp1 is None:
        print("Open failed.\n")
    layer1 = shp1.GetLayer()
    extent1 = layer1.GetExtent()

    # Open the second shapefile with OGR, with read only access
    shp2 = driver.Open(str(inshp2), 0)  # 0 means read-only. 1 means writeable.
    if shp2 is None:
        print("Open failed.\n")
    layer2 = shp2.GetLayer()

    # Create a Polygon from the extent tuple
    ring = ogr.Geometry(ogr.wkbLinearRing)
    ring.AddPoint(extent1[0], extent1[2])
    ring.AddPoint(extent1[1], extent1[2])
    ring.AddPoint(extent1[1], extent1[3])
    ring.AddPoint(extent1[0], extent1[3])
    ring.AddPoint(extent1[0], extent1[2])
    poly = ogr.Geometry(ogr.wkbPolygon)
    poly.AddGeometry(ring)
    layer2.SetSpatialFilter(poly)
    poly = None

    # Set up transformation in case input is not in WGS84
    # DOES NOT WORK FOR EPSG 3034 and 4623

    coordinateSystem = osr.SpatialReference()
    coordinateSystem.ImportFromWkt(layer2.GetSpatialRef().ExportToWkt())  # Use projection from input shapefile2
    #src_epsg = pyproj.CRS.from_wkt(layer2.GetSpatialRef().ExportToWkt()).to_epsg()
    src_wkt = layer2.GetSpatialRef().ExportToWkt()
    """
    point_ref = osr.SpatialReference()
    geoSR = osr.SpatialReference()
    point_ref.ImportFromEPSG(4326)  # WGS84
    if int(osgeo.__version__[0]) >= 3:
        # GDAL 3 changes axis order: https://github.com/OSGeo/gdal/issues/1546
        point_ref.SetAxisMappingStrategy(osr.OAMS_TRADITIONAL_GIS_ORDER)
    ctran = osr.CoordinateTransformation(coordinateSystem, point_ref)  # Create transformation for converting to WGS84
    """

    # Check for existence of provided fieldnames
    field_defn1, fieldslist1 = checkfield(layer1, fieldname1, 'shapefile1')
    field_defn2, fieldslist2 = checkfield(layer2, fieldname2, 'shapefile2')

    # Get information about field types for building the output NetCDF file later
    fieldtype1 = getfieldinfo(field_defn1, fieldname1)
    fieldtype2 = getfieldinfo(field_defn2, fieldname2)

    # Load entire polygon shapefile into shapely to create a spatial filter
    if gridflag == 1:
        polys2 = [(feat.GetField(fieldname2), loads(feat.GetGeometryRef().ExportToWkb()), feat.GetField('lon_cen'),
                   feat.GetField('lat_cen'), feat.GetField('i_index'), feat.GetField('j_index')) for feat in layer2]
    elif gridflag == 0:
        polys2 = [(feat.GetField(fieldname2), loads(feat.GetGeometryRef().ExportToWkb())) for feat in layer2]
    layer2.ResetReading()

    if len(polys2) > 1:  # Use cascaded union geometry to limit results using spatial filter on layer1
        shgeom = cascaded_union([poly[1] for poly in polys2])
    else:
        shgeom = polys2[0][1]
    geom = ogr.CreateGeometryFromWkb(shgeom.wkb)  # Create OGR feature from Shapely geometry
    print("    Cascaded union completed on shapefile2. Setting spatial filter...")

    # Get list of unique fieldnames in input field for shapefile1
    # This method goes by all features sharing a common field value (like gage_id) - takes longer with unique values than without
    layer1.SetSpatialFilter(geom)
    trials = layer1.GetFeatureCount()
    valuelist = []
    feature = layer1.GetNextFeature()
    while feature is not None:
        valuelist.append(feature.GetField(fieldname1))
        feature.Destroy()
        feature = layer1.GetNextFeature()
    uniquevaluelist1 = list(set(valuelist))  # Creating a set eliminates duplicates
    layer1.ResetReading()
    print('    Finished gathering %s unique fieldnames for shapefile1...' % len(uniquevaluelist1))

    # loop through the unique values of input features to create geometry list
    polys1 = []
    for value in uniquevaluelist1:
        layer1.SetAttributeFilter("%s = '%s'" % (fieldname1, value))
        polys = [loads(feat.GetGeometryRef().ExportToWkb()) for feat in layer1]
        layer1.ResetReading()
        if len(polys) > 1:  # Handle situations where only one goemetry exists
            polys1.append((value, cascaded_union(polys)))
            print("        Adding polygon %s to polygons list and performing cascaded union." % value)
        else:
            polys1.append((value, polys[0]))
            print("        Adding polygon %s to polygons list." % value)

    ##    # This spatial filter will limit layer 1 to the unionized features in layer 2
    ##    layer1.SetSpatialFilter(geom)
    ##    trials = layer1.GetFeatureCount()
    ##    polys1 = [(feat.GetField(fieldname1), loads(feat.GetGeometryRef().ExportToWkb())) for feat in layer1]
    ##    layer1.ResetReading()
    ##    print "        Done gathering polygons from shapefile2."

    # Loop through the basin polygons
    invalidcount = 0
    for polygon in polys1:
        print("        Gathering correspondence information for polygon %s" % polygon[0])
        shgeom = polygon[1]
        polygon_area = shgeom.area

        try:
            # Attempt to find all overlapping pixels
            if gridflag == 1:  # Read centroid info from attribute table instead of from geometry
                pixelweights = [
                    (poly[0], (poly[1].intersection(shgeom).area / polygon_area), poly[2], poly[3], poly[4], poly[5])
                    for poly in polys2 if shgeom.intersects(poly[1]) == True]
            elif gridflag == 0:
                """
                pixelweights = [(poly[0], (poly[1].intersection(shgeom).area / polygon_area),
                                 ctran.TransformPoint(poly[1].centroid.x, poly[1].centroid.y)[0],
                                 ctran.TransformPoint(poly[1].centroid.x, poly[1].centroid.y)[1]) for poly in polys2 if
                                shgeom.intersects(poly[1]) == True]
                """
                pixelweights = []
                for poly in polys2:
                    if shgeom.intersects(poly[1]) == True:
                        lon, lat = reproject_point(poly[1].centroid.x, poly[1].centroid.y, src_wkt, 4326,
                                                   src_type='wkt', tar_type='epsg',
                                                   always_xy=True)
                        pixelweights.append((poly[0], (poly[1].intersection(shgeom).area / polygon_area), lon, lat))
            spatialweights[polygon[0]] = pixelweights
        except:
            invalidcount += 1
            pass

    print(    "    Invalid Count: %s" % invalidcount)
    print(    "    %s trials completed in %s seconds" % (trials, time.time() - tic))
    return spatialweights, fieldtype1, fieldtype2


def create_ragged_ncfile(spatialweights, outputfile, fieldtype1, fieldtype2, gridflag=0):
    '''
    Function courtesy of NCAR, supplied by Andrew Newman (April 2021), written by
    Kevin Sampson
    Associate Scientist
    National Center for Atmospheric Research
    ksampson@ucar.edu
    This function creates a ragged-array netCDF file, with variable length dimensions
    for each basin.
    '''

    # Create netcdf file for this simulation
    rootgrp = netCDF4.Dataset(outputfile, 'w', format='NETCDF4')

    # Create variable length (ragged) data type
    vlen_i = rootgrp.createVLType(np.int32, 'vlen_int')
    vlen_f = rootgrp.createVLType(np.float64, 'vlen_float')

    # Create dimensions and set other attribute information
    dim1 = 'polyid'
    dim = rootgrp.createDimension(dim1, len(spatialweights))

    # Create variable-length variables
    weights = rootgrp.createVariable('weight', vlen_f, (dim1))                  # (64-bit floating point)
    lats = rootgrp.createVariable('latitude', vlen_f, (dim1))                   # (64-bit floating point)
    lons = rootgrp.createVariable('longitude', vlen_f, (dim1))                  # (64-bit floating point)
    if fieldtype2 == 'integer':
        ids2 = rootgrp.createVariable('intersector', vlen_i, (dim1))            # Coordinate Variable (32-bit signed integer)
    elif fieldtype2 == 'integer64':
        ids2 = rootgrp.createVariable('intersector', vlen_i, (dim1))
    elif fieldtype2 == 'string':
        ids2 = rootgrp.createVariable('intersector', vlen_f, (dim1))            # Coordinate Variable (string type character)

    if gridflag == 1:
        iindex = rootgrp.createVariable('i_index', vlen_i, (dim1))              # (32-bit signed integer)
        jindex = rootgrp.createVariable('j_index', vlen_i, (dim1))              # (32-bit signed integer)
        iindex.long_name = 'Index in the x dimension of the raster grid (starting with 0,0 in UL corner)'
        jindex.long_name = 'Index in the y dimension of the raster grid (starting with 0,0 in UL corner)'

    # Handle the data type of the polygon identifier
    if fieldtype1 == 'integer':
        ids = rootgrp.createVariable(dim1,'i4',(dim1))                          # Coordinate Variable (32-bit signed integer)
    elif fieldtype1 == 'integer64':
        ids = rootgrp.createVariable(dim1,'i8',(dim1))                          # Coordinate Variable (64-bit signed integer)
    elif fieldtype1 == 'string':
        ids = rootgrp.createVariable(dim1,str,(dim1))                           # Coordinate Variable (string type character)

    # Create fixed-length variables
    overlaps = rootgrp.createVariable('overlaps','i8',(dim1))                   # 64-bit signed integer

    # Set variable descriptions
    weights.long_name = 'fraction of polygon(polyid) intersected by polygon identified by poly2'
    ids.long_name = 'ID of polygon'
    overlaps.long_name = 'Number of intersecting polygons'
    lats.long_name = 'Centroid latitude of intersecting polygon in degrees north in WGS84 EPSG:4326'
    lons.long_name = 'Centroid longitude of intersecting polygon in degrees east in WGS84 EPSG:4326'
    ids2.long_name = 'ID of the polygon that intersects polyid'

    # Set units
    lats.units = 'degrees north'
    lons.units = 'degrees east'

    # Fill in global attributes
    rootgrp.history = 'Created %s' %time.ctime()

    spatialweights_keys = list(spatialweights.keys())
    # Start filling in elements
    if fieldtype1 == 'integer':
        ids[:] = np.array(spatialweights_keys)                         # Ths method works for int-type netcdf variable
    elif fieldtype1 == 'integer64':
        ids[:] = np.array(spatialweights_keys)                         # Ths method works for int64-type netcdf variable
    elif fieldtype1 == 'string':
        ids[:] = np.array(spatialweights_keys, dtype=np.object)     # This method works for a string-type netcdf variable

    #ids[:] = np.array([str(x) for x in spatialweights.keys()], dtype=np.object)   # This method will convert to string-type netcdf variable from int
    overlaps[:] = np.array([len(x) for x in spatialweights.values()])
    ids2[:] = np.array([np.array([x[0] for x in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)
    weights[:] = np.array([np.array([weight[1] for weight in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)
    lons[:] = np.array([np.array([x[2] for x in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)
    lats[:] = np.array([np.array([x[3] for x in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)

    if gridflag == 1:
        iindex[:] = np.array([np.array([x[4] for x in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)
        jindex[:] = np.array([np.array([x[5] for x in spatialweights[spatialweights_keys[i]]]) for i in range(len(spatialweights_keys))], dtype=np.object)

    # Close file
    rootgrp.close()


def create_ncfile(spatialweights, outputfile, fieldtype1, fieldtype2, gridflag=0):
    '''
    Function courtesy of NCAR, supplied by Andrew Newman (April 2021), written by
    Kevin Sampson
    Associate Scientist
    National Center for Atmospheric Research
    ksampson@ucar.edu
    This function creates an netcdf file with two dimensions ('hruid' and 'gridoverlaps')
    which describe each basin and the grid cells that intersect it.  This leaves
    'whitespace' or blank index values and takes up unecessary disk space.'''

    # Create netcdf file for this simulation
    rootgrp = netCDF4.Dataset(outputfile, 'w', format='NETCDF4')

    # Get length of largest grid to basin overlap
    overlaps = max([len(x) for x in spatialweights.values()])

    # Create dimensions and set other attribute information
    dim1 = 'polyid'
    dim2 = 'gridoverlaps'
    dim = rootgrp.createDimension(dim1, len(spatialweights))
    overlap = rootgrp.createDimension(dim2, overlaps)

    # Handle the data type of the polygon identifier
    if fieldtype1 == 'integer':
        ids = rootgrp.createVariable(dim1,'i4',(dim1))                          # Coordinate Variable (32-bit signed integer)
    elif fieldtype1 == 'integer64':
        ids = rootgrp.createVariable(dim1, 'i8', (dim1))                        # Coordinate Variable (64-bit signed integer)
    elif fieldtype1 == 'string':
        ids = rootgrp.createVariable(dim1,str,(dim1))                           # Coordinate Variable (string type character)

    if fieldtype2 == 'integer':
        ids2 = rootgrp.createVariable('intersector', 'i4', (dim1,dim2))         # (32-bit signed integer)
    elif fieldtype2 == 'integer64':
        ids2 = rootgrp.createVariable('intersector', 'i8', (dim1,dim2))          # (64-bit signed integer)
    elif fieldtype2 == 'string':
        ids2 = rootgrp.createVariable('intersector', str, (dim1,dim2))          # (string type character)

    # Create fixed-length variables
    overlapers = rootgrp.createVariable('overlap','i4',(dim2))                  # Coordinate Variable (32-bit signed integer)
    weights = rootgrp.createVariable('weight', 'f8', (dim1,dim2))               # (64-bit floating point)
    lats = rootgrp.createVariable('latitude', 'f8',(dim1,dim2))                 # (64-bit floating point)
    lons = rootgrp.createVariable('longitude', 'f8',(dim1,dim2))                # (64-bit floating point)
    overlapps = rootgrp.createVariable('overlaps','i4',(dim1))                  # (32-bit signed integer)

    if gridflag == 1:
        iindex = rootgrp.createVariable('i_index', 'i4', (dim1,dim2))           # (32-bit signed integer)
        jindex = rootgrp.createVariable('j_index', 'i4', (dim1,dim2))           # (32-bit signed integer)
        iindex.long_name = 'Index in the x dimension of the raster grid (starting with 0,0 in UL corner)'
        jindex.long_name = 'Index in the y dimension of the raster grid (starting with 0,0 in UL corner)'

    # Set variable descriptions
    weights.long_name = 'fraction of polygon(polyid) intersected by polygon identified by poly2'
    ids.long_name = 'ID of polygon'
    overlapers.long_name = 'Overlap number (1-n)'
    lats.long_name = 'Centroid latitude of intersecting polygon in degrees in WGS84 EPSG:4326'
    lons.long_name = 'Centroid longitude of intersecting polygon in degrees in WGS84 EPSG:4326'
    overlapps.long_name = 'Number of intersecting polygons'
    ids2.long_name = 'ID of the polygon that intersects polyid'

    # Set units
    lats.units = 'degrees'
    lons.units = 'degrees'

    # Fill in global attributes
    rootgrp.history = 'Created %s' %time.ctime()

    # Start filling in elements
    if fieldtype1 == 'integer':
        ids[:] = np.array(list(spatialweights.keys()))                    # This method works for int-type netcdf variable
    elif fieldtype1 == 'integer64':
        ids[:] = np.array(list(spatialweights.keys()))
    elif fieldtype1 == 'string':
        ids[:] = np.array(list(spatialweights.keys()), dtype=np.object)     # This method works for a string-type netcdf variable

    overlapers[:] = np.array(range(overlaps))
    overlapps[:] = np.array([len(x) for x in spatialweights.values()])
    spatialweights_keys = list(spatialweights.keys())
    for i in range(len(spatialweights_keys)):                                 # For each basin
        weightslist = [weight for weight in spatialweights[spatialweights_keys[i]]]           # Generate list of gridcell IDs and spatial weights
        for x in weightslist:                                                   # For each grid cell in each basin
            ids2[i, weightslist.index(x)] = x[0]
            weights[i, weightslist.index(x)] = x[1]
            lons[i, weightslist.index(x)] = x[2]
            lats[i, weightslist.index(x)] = x[3]
            if gridflag == 1:
                iindex[i, weightslist.index(x)] = x[4]
                jindex[i, weightslist.index(x)] = x[5]

    # Close file
    rootgrp.close()


def gridtobasin_function(infeatures1, fieldname1, infeatures2, fieldname2, gridflag=0, outputnc=None, raggednc=False):
    """
    Function courtesy of NCAR, supplied by Andrew Newman (April 2021), written by
    Kevin Sampson
    Associate Scientist
    National Center for Atmospheric Research
    ksampson@ucar.edu
      <Polygon1>     -> Polygon Shapefile (full path)
      <FieldName1>   -> Polygon identifier field name for Polygon1
      <Polygon2>     -> Polygon Shapefile2 (full path)
      <FieldName2>   -> Polygon identifier field name for Polygon 2
      <gridflag>     -> Optional - Type 'GRID' to indicate Polygon2 was generated from grid2shp.py
      <outputNC>     -> Optional - Full path to output netCDF file (.nc extentsion required)
      <VLENflag>     -> Optional - Type 'VLEN' to indicate output NC file should be variable length array type.
    :return: 
    """
    infeatures1 = str(infeatures1)
    infeatures2 = str(infeatures2)
    # Input directories
    #indir = os.path.dirname(__file__)                                           # Store current working directory
    indir = os.path.dirname(infeatures2)                                        # Use directory of output file
    print('Input Directory: %s' %indir)

    # Output Files
    if outputnc is None:
        outncFile = os.path.join(indir, os.path.basename(infeatures1)[:-4]+'.nc')
    else:
        outncFile = outputnc


    if os.path.isfile(outncFile) == True:
        print( "Removing existing file: %s" %outncFile)
        os.remove(outncFile)

    # Step 2: Calculate per gridcell feature coverage fractions
    tic1 = time.time()
    print( "Step 2 starting.")
    spatialweights, fieldtype1, fieldtype2 = spatial_weights(infeatures1, infeatures2, fieldname1, fieldname2, gridflag)
    print( "Step 2 completed in %s seconds." %(time.time()-tic1))

    # Step 3: Write weights to ragged array netcdf file
    tic1 = time.time()
    print( "Step 3 starting.")
    if raggednc == True:
        create_ragged_ncfile(spatialweights, outncFile, fieldtype1, fieldtype2, gridflag)
    else:
        create_ncfile(spatialweights, outncFile, fieldtype1, fieldtype2, gridflag)
    print( "Step 3 completed in %s seconds." %(time.time()-tic1))

    # Remove intermediate projected polygons
    if os.path.isfile(infeatures2) == True:
        print( "Removing intermediate projected shapefile: %s" %infeatures2)
        remover = [os.remove(infile) for infile in glob.glob(infeatures2.replace('.shp', '.*'))]

    # Step 4: Area weighted statistics are then calculated for each feature using the grid values and fractions as weights
    # Step 5: Repeat Step 4 for each timestep in the input gridded dataset
    print( "Total time elapsed: %s seconds." %(time.time()-tic1))

