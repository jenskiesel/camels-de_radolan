# This script is the main file to run the workflow of the RADOLAN processing
#
# Author:
# Jens Kiesel - jenskiesel@gmail.com
#
# Python 3.6.6
import config as c
import log as lg
import hf
import RADOLAN_01_download_fromHTTPS as rad_dwn
import RADOLAN_02_convert_to_NetCDF as rad_cnv


class ProcessingControl(object):
    """Class handles model & data flow"""

    def __init__(self):
        lg.prtinf(f'Initating RADOLAN Processing', c.LOG)

    def processing(self):

        if c.DOWNLOAD_RADOLAN_DATA:
            lg.prtinf(f'Downloading radolan data', c.LOG)
            # run the download script
            rad_dwn.download(years=c.RADOLAN_YEARS,
                             years_of_recent_data=c.RADOLAN_YRS_RECENT_DATA,
                             base_url=c.RADOLAN_BASE_URL,
                             url_sep=c.RADOLAN_URL_SEP,
                             recent_subfol=c.RADOLAN_RECENT_SUBFOL,
                             historical_subfol=c.RADOLAN_HISTORICAL_SUBFOL,
                             err_fp=c.RADOLAN_DWL_ERR_FP,
                             out_data_fol=c.RADOLAN_DWL_F,
                             new_only=c.NEW_ONLY_OPTION)

        if c.CONVERT_RADOLAN_TO_NC:
            lg.prtinf(f'Convert radolan data to NetCDF', c.LOG)
            # merge all radolan files and convert to NetCDF
            gz_fps = rad_cnv.get_file_paths_from_folder(c.RADOLAN_DWL_F, 'gz')
            for i, gz_fp in enumerate(gz_fps):
                hf.print_progress_bar(i, len(gz_fps))
                # convert each day's radolan data into one NetCDF file and place in the folder for single NetCDFs
                nc_fp = c.RADOLAN_MULTIPLE_NCS_F / f'{gz_fp.stem.split(".")[0]}{c.RADOLAN_NC_EXT}'
                rad_cnv.hourly_radolan_data_to_netcdf(shp_fp=c.BASINS_SHP_FP,
                                                      gz_fp=gz_fp,
                                                      force_minutes=c.RADOLAN_FORCE_MINUTES,
                                                      src_proj=c.SRC_PROJ,
                                                      src_proj_type=c.SRC_PROJ_TYPE,
                                                      tar_proj4=c.RADOLAN_PROJ4,
                                                      out_nc_fp=nc_fp)
            # merge all NetCDFs into one covering the whole time series and get the file name of that NetCDF
            return rad_cnv.merge_to_one_nc(multiple_ncs_f=c.RADOLAN_MULTIPLE_NCS_F,
                                                    out_f_single_nc=c.RADOLAN_NC_F,
                                                    out_nc_base_name=c.RADOLAN_NC_BASENAME,
                                                    out_nc_ext=c.RADOLAN_NC_EXT)


if __name__ == '__main__':
    # initiate control
    run = ProcessingControl()
    # run workflow
    run.processing()

