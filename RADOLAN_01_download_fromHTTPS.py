import urllib.request
import datetime



errors = []   # keeps track of erroneous downloads
months_str = ["01", "02", "03", "04","05","06","07","08","09","10","11","12"]
days_str = ["01", "02", "03", "04","05","06","07","08","09","10","11","12",
			"13", "14", "15", "16","17","18","19","20","21","22","23","24",
			"25", "26", "27", "28","29","30","31"]

def download_file(url, out_fp):
	global errors
	try:
		urllib.request.urlretrieve(url, out_fp)
		print("Downloaded: " + str(out_fp))
	except:
		errors.append(url)

def download(years, years_of_recent_data, base_url, url_sep, recent_subfol, historical_subfol, err_fp, out_data_fol, new_only):
	for year in years:
		for mon_str in months_str:
				if year in years_of_recent_data:
					for day_str in days_str:
						url_folder = base_url + url_sep + recent_subfol
						date = f'{year}{mon_str}{day_str}'
						try:
							datetime.datetime.strptime(date, '%Y%m%d')
						except ValueError:
							continue  # not a valid date, skip to next
						file = f'RW-{date}.tar.gz'
						out_fp = out_data_fol / file
						if new_only:
							if out_fp.exists():
								continue
							else:
								download_file(url_folder + url_sep + file, out_fp)
				else:
					url_folder = base_url + url_sep + historical_subfol + url_sep + str(year)
					file = f'RW-{year}{mon_str}.tar'
					out_fp = out_data_fol / file
					if new_only:
						if out_fp.exists():
							continue
					download_file(url_folder + url_sep + file, out_fp)

	if len(errors)>0:
		with open(err_fp, 'w') as wf:
			wf.write("ERRORS:")
			'\n'.join(errors)
