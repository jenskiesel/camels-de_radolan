# This script is part of the workflow to download DWD RADOLAN
# data and convert to NetCDF for a mask shapefile
#
#
# Author:
# Jens Kiesel - jenskiesel@gmail.com
#
# Python 3.6.6
import os
import datetime
from pathlib import Path

import log as lg


VERSION = 0.1

# date and time
START_TIME = datetime.datetime.now()
DATE_FORMAT = '%Y%m%d'
DATE_STR = START_TIME.strftime('%y%m%d%H%M%S')

# logging
BASE_FOLDER = Path().absolute()
LOG_PATH = BASE_FOLDER / '#log_files'
LOG_FILE_NAME = 'data_processing_logging.log'
LOGGER_NAME = 'LOG'

# paths to files
BASINS_SHP_FP = Path(r'c:\Users\jensk\Work\01_Projects\210301bsu_DFG_RESIST\03_Data\Kinzig\Masks\Kinzig_Mask_SWAT_25832.shp')
SRC_PROJ = None  # '+proj=stere +lat_0=90.0 +lon_0=10.0 +lat_ts=60.0 +a=6370040 +b=6370040 +units=m'  # SRC_PROJ needed if source shapefile is not projected in a known coordinate system!
SRC_PROJ_TYPE = 'proj4'  # 'epsg'
BASIN_ID_FIELD = "Subbasin"  # the field of the polygon file defining the areas for which data will be averaged

# global variables
CPUS = min([1, os.cpu_count()])
PRINT_TO_CONSOLE = False if CPUS > 1 else True
NO_DATA = -99.0
MAX_CONSECUTIVE_NANS_INTERPOL = 5  # maximum number of no data values in time series that will be linearly interpolated
TAR_PROJ = '+proj=stere +lat_0=90.0 +lon_0=10.0 +lat_ts=60.0 +a=6370040 +b=6370040 +units=m'
TAR_PROJ_TYPE = 'proj4'

# processing flags
DOWNLOAD_RADOLAN_DATA = True
NEW_ONLY_OPTION = False  # if true, only newly available files are downloaded
CONVERT_RADOLAN_TO_NC = True
RADOLAN_TO_BASIN = True

# radolan processing
RADOLAN_DWL_F = Path(r'C:\Temp')
RADOLAN_BASE_FOL = Path(r'C:\Temp\Climate\RADOLAN')
RADOLAN_NC_F = RADOLAN_BASE_FOL / '01_NetCDFCut'
RADOLAN_GRID2BASIN_FOLDER = RADOLAN_BASE_FOL / f'02_grid2basin_{BASINS_SHP_FP.stem}'  # will contain the basin2grid files
RADOLAN_WEIGHT_FP = RADOLAN_GRID2BASIN_FOLDER / f'weight_grid2basin_{BASINS_SHP_FP.stem}.nc'
RADOLAN_RESULTS_F = RADOLAN_BASE_FOL / f'03_csvFiles_{BASINS_SHP_FP.stem}'  # the output folder in which the .csv time series are saved

RADOLAN_MULTIPLE_NCS_F = RADOLAN_NC_F / 'all_nc_files'  # the folder with all radolan netcdfs cut to the region of interest
RADOLAN_PROJ4 = '+proj=stere +lat_0=90.0 +lon_0=10.0 +lat_ts=60.0 +a=6370040 +b=6370040 +units=m'  # from https://opendata.dwd.de/climate_environment/CDC/help/RADOLAN/Unterstuetzungsdokumente/Unterstuetzungsdokumente-Verwendung_von_RADOLAN-Produkten_im_ASCII-GIS-Rasterformat_in_GIS.pdf
RADOLAN_NC_EXT = '.nc'

# radolan downloading
RADOLAN_BASE_URL = "https://opendata.dwd.de/climate_environment/CDC/grids_germany/hourly/radolan"
RADOLAN_HISTORICAL_SUBFOL = 'historical/asc'
RADOLAN_RECENT_SUBFOL = 'recent/asc'
RADOLAN_URL_SEP = '/'
# the data that is located in the 'recent' subfolder at the RADOLAN URL! (CHECK BEFORE RUNNING)
# also, check if older files have been updated and download those manually!!
RADOLAN_YRS_RECENT_DATA = [2021, 2022]
RADOLAN_YEARS = range(2005, 2022)
RADOLAN_DWL_ERR_FP = RADOLAN_DWL_F / '#download_errors.txt'

# radolan extraction
RADOLAN_VAR = 'precip'  # 'pr'
RADOLAN_NC_LAT_STR = 'y'  # 'lat'  # not consistently used - mostly hardcoded in functions
RADOLAN_NC_LON_STR = 'x'  # 'lon'  # not consistently used - mostly hardcoded in functions
RADOLAN_NC_TIME_STR = 'time'
RADOLAN_NC_BASENAME = 'RW_Kinzig'
RADOLAN_FORCE_MINUTES = '50'  # to force all time stamps of each hour to the same minute (replace 45)
RADOLAN_VAR_CONVERSION = {'precip':[0.1, '*']}  # must match the vars names
RADOLAN_NAN_STATS_FP = RADOLAN_RESULTS_F / '#nan_stats.csv'
OUT_TIME_STEP = 'D'  # 'H'
OUT_DATA_TYPE = 'swat'

# settings for swat outputs
START_DATE = '2005-06-01'
END_DATE = '2021-12-31'

# create folders if not existing
RADOLAN_DWL_F.mkdir(parents=True, exist_ok=True)
RADOLAN_NC_F.mkdir(parents=True, exist_ok=True)
RADOLAN_RESULTS_F.mkdir(parents=True, exist_ok=True)
RADOLAN_MULTIPLE_NCS_F.mkdir(parents=True, exist_ok=True)
LOG_PATH.mkdir(parents=True, exist_ok=True)
RADOLAN_GRID2BASIN_FOLDER.mkdir(parents=True, exist_ok=True)

# remove files
if RADOLAN_NAN_STATS_FP.exists():
    RADOLAN_NAN_STATS_FP.unlink()

# logging
LOG_LEVEL = 'info'
LOG_FP = BASE_FOLDER / '#log_files' / 'data_processing_logging.log'
LOGGER_NAME = 'LOG'
LOG = lg.setup_logging(LOG_FP, LOGGER_NAME, version=VERSION)
